import json
import base64
import hmac,hashlib

#jwk的流程
#1.token的签发
#2.token的验证

#1.token的签发
#token包括三部分，头部header、载荷payload、签证signature
#头部加密
header = {
  'typ': 'JWT',
  'alg': 'HS256'
}
#将头部信息通过编码进行转译
json_header = json.dumps(header).encode()
b_json_header = base64.b64encode(json_header)

#载荷信息
payload = {
    "user_id": 4,
    "age": 18,
    "name": "weiwei",
    "admin": True
}
#载荷信息同样通过编译方式进行转化
b_json_payload = base64.b64encode(json.dumps(payload).encode())
#b'eyJ0eXAiOiAiSldUIiwgImFsZyI6ICJIUzI1NiJ9'   header (base64后的)
#b'eyJ1c2VyX2lkIjogNCwgImFnZSI6IDE4LCAibmFtZSI6ICJ3ZWl3ZWkiLCAiYWRtaW4iOiB0cnVlfQ=='  payload (base64后的)
#签证信息
#签证信息由三部分组成：header (base64后的) \ payload (base64后的) \ secret(密钥)
# 原数据massage = header + '.' + payload
# 1、构建哈希对象
massage = b_json_header + b'.' + b_json_payload
secret_key = b'm2%u^o!fv+nfil^=y$-5qj+(@7whf*+nz!wzyb2^89%t=!q0wr'
h_obj = hmac.new(secret_key,msg=massage,digestmod=hashlib.sha256)
#2、通过哈希对象，得出签名(信息摘要)
signature = h_obj.hexdigest()
print('signature:',signature)
#signature:165cd5754b81f250786d64930ee8408807a6f4b117c1879f713098873ccfdd0b
#拼接出完整的token
token = b_json_header.decode()+'.'+b_json_payload.decode()+'.'+signature
print('token:',token)
#eyJ0eXAiOiAiSldUIiwgImFsZyI6ICJIUzI1NiJ9.eyJ1c2VyX2lkIjogNCwgImFnZSI6IDE4LCAibmFtZSI6ICJ3ZWl3ZWkiLCAiYWRtaW4iOiB0cnVlfQ==.165cd5754b81f250786d64930ee8408807a6f4b117c1879f713098873ccfdd0b

#验证
#1.接收从浏览器传来的请求头token信息
token_from_browser = token

#2.获取header和payload，signature
token_list = token_from_browser.split('.')
json_header_check = token_list[0]
json_payload_check = token_list[1]
signature_check = token_list[2]
#3.将header和payload按签发时的加密方法和密钥进行加密，得到签证
massage_check = json_header_check + '.' + json_payload_check
h_obj = hmac.new(secret_key,msg=massage,digestmod=hashlib.sha256)
signature = h_obj.hexdigest()
#4.查看该签证和前端传来的signature是否匹配，如果匹配则证明未篡改
if signature == signature_check:
    print("数据完整！")
    user_info = base64.b64decode(json_payload_check.encode()).decode()
    print(user_info)
else:
    print("数据被篡改了！")