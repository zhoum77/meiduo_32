from goods.models import GoodsChannel


def get_category():
    """获取展示商品频道分类"""
    category = {}
    goods_channel_qs = GoodsChannel.objects.all().order_by('group_id', 'sequence')
    for goods_channel in goods_channel_qs:
        group_id = goods_channel.group_id
        if group_id not in category:
            category[group_id] = {'channels': [], 'sub_cats': []}
        cat1 = goods_channel.category  # 外键多对一，得到GoodsCategory中的一级标题对象
        cat1.url = goods_channel.url
        category[group_id]['channels'].append(cat1)
        cat2_qs = cat1.subs.all()
        for cat2 in cat2_qs:
            cat2.sub_cats = cat2.subs.all()
            category[group_id]['sub_cats'].append(cat2)
    return category