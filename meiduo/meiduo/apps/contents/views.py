from django.shortcuts import render
from django.views import View
from contents.models import ContentCategory

from .utils import get_category
# Create your views here.
#首页视图
class IndexView(View):
    def get(self,request):
        contents = {}
        content_qs = ContentCategory.objects.all()
        for content in content_qs:
            contents[content.key] = content.content_set.filter(status=True)        #一查多，一的模型对象.多的小写类名_set.all(),过滤展示的广告
        context = {
            'categories':get_category(),  #展示商品频道分类
            'contents':contents,      #展示首页商品广告
        }
        return render(request,'index.html',context)
#<QuerySet [<GoodsChannel: 手机>, <GoodsChannel: 相机>, <GoodsChannel: 数码>, <GoodsChannel: 电脑>, <GoodsChannel: 办公>, <GoodsChannel: 家用电器>, <GoodsChannel: 家居>, <GoodsChannel: 家具>, <GoodsChannel: 家装>, <GoodsChannel: 厨具>, <GoodsChannel: 男装>, <GoodsChannel: 女装>, <GoodsChannel: 童装>, <GoodsChannel: 内衣>, <GoodsChannel: 女鞋>, <GoodsChannel: 箱包>, <GoodsChannel: 钟表>, <GoodsChannel: 珠宝>, <GoodsChannel: 男鞋>, <GoodsChannel: 运动>, '...(remaining elements truncated)...']>
# """
# {
#
#     '1': {
#         'channles': [cat1, cat1, cat1],
#         'sub_cats': [cat2, cat2, cat2],
#     },
#
#     '2': {
#         'channles': [cat1, cat1, cat1],
#         'sub_cats': [cat2, cat2, cat2],
#     },
#     '''
#
# }
# """

# {
#     'category':
#         {
#             1: {'channels': [{'name': <GoodsCategory: 手机>, 'url': 'http://shouji.jd.com'}, {'name': <GoodsCategory: 相机>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 数码>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []},
#             2: {'channels': [{'name': <GoodsCategory: 电脑>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 办公>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 家用电器>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 3: {'channels': [{'name': <GoodsCategory: 家居>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 家具>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 家装>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 厨具>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 4: {'channels': [{'name': <GoodsCategory: 男装>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 女装>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 童装>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 内衣>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 5: {'channels': [{'name': <GoodsCategory: 女鞋>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 箱包>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 钟表>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 珠宝>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 6: {'channels': [{'name': <GoodsCategory: 男鞋>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 运动>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 户外>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 7: {'channels': [{'name': <GoodsCategory: 房产>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 汽车>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 汽车用品>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 8: {'channels': [{'name': <GoodsCategory: 母婴>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 玩具乐器>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 9: {'channels': [{'name': <GoodsCategory: 食品>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 酒类>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 生鲜>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 特产>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 10: {'channels': [{'name': <GoodsCategory: 图书>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 音像>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 电子书>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}, 11: {'channels': [{'name': <GoodsCategory: 机票>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 酒店>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 旅游>, 'url': 'http://www.itcast.cn'}, {'name': <GoodsCategory: 生活>, 'url': 'http://www.itcast.cn'}], 'sub_cats': []}}}
# <class 'dict'>: {1:
#                      {
#                          'channels': [<GoodsCategory: 手机>, <GoodsCategory: 相机>, <GoodsCategory: 数码>],
#                         'sub_cats': [<GoodsCategory: 手机通讯>, <GoodsCategory: 手机配件>, <GoodsCategory: 摄影摄像>, <GoodsCategory: 数码配件>, <GoodsCategory: 影音娱乐>, <GoodsCategory: 智能设备>, <GoodsCategory: 电子教育>]
# },
