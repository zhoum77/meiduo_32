from django.conf.urls import url
from .views import IndexView
urlpatterns = [
    url(r'^$',IndexView.as_view())   #首页路由不需要加‘/’，浏览器会自动解析，加了反而报错
]