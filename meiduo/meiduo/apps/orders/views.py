from django.shortcuts import render
from django.views import View
from django.http import JsonResponse,HttpResponseForbidden
from django_redis import get_redis_connection
from django.utils import timezone
from django.db import transaction
from django.core.paginator import Paginator
import logging
from meiduo.utils.views import LoginRequireView
from users.models import Address,User
from goods.models import SKU
from meiduo.utils.response_code import RETCODE
from .models import OrderGoods,OrderInfo
from decimal import Decimal
import json,time
# Create your views here.

class OrderSettlementView(LoginRequireView):
    """结算订单"""
    def get(self,request):
        #1.获取数据
        #2.校验数据
        #3.业务逻辑处理
        #非登录用户跳转到登录页面,登录用户才会渲染订单处理的页面
        user = request.user  #获取登录用户对象
        # addresses_qs = user.addresses.filter(is_delete=False)
        addresses_qs = Address.objects.filter(user=user,is_deleted=False)  #获取用户所有的收货地址
        #获取所有的选中商品信息
        redis_conn = get_redis_connection('carts')
        redis_carts = redis_conn.hgetall('cart_user%s'%user.id)
        redis_selecteds = redis_conn.smembers('selected_user%s'%user.id)
        skus = []
        total_count = 0
        total_amount = 0
        for sku_id_byte in redis_selecteds:
            sku = SKU.objects.get(id=sku_id_byte)
            count = int(redis_carts[sku_id_byte])
            amount = (sku.price)*count
            sku.count = count
            sku.amount = amount
            skus.append(sku)
            total_count += count
            total_amount += amount
        payment_amount = total_amount + 10
        #4.渲染页面
        context = {
            'addresses':addresses_qs,
            'skus':skus,
            'total_count':Decimal(total_count),
            'total_amount':Decimal(total_amount),
            'freight':Decimal('10.00'),
            'payment_amount':Decimal(payment_amount),
        }
        return render(request,'place_order.html',context)


class OrderCommitView(LoginRequireView):
    """提交订单"""
    def post(self,request):
        #1.接收数据
        json_dict = json.loads(request.body.decode())
        address_id = json_dict.get('address_id')
        pay_method = json_dict.get('pay_method')
        #2.校验数据
        #判断这个地址是否为该登录用户的有效地址
        user = request.user
        try:
            address = Address.objects.get(user=user,id=address_id,is_deleted=False)
        except Address.DoesNotExist:
            return HttpResponseForbidden('无效地址')
        #判断支付方式是否有效，和创建的表匹配
        if pay_method not in (OrderInfo.PAY_METHODS_ENUM['CASH'],OrderInfo.PAY_METHODS_ENUM['ALIPAY']):
            return HttpResponseForbidden('支付方式无效')
        #3.业务逻辑处理
        #3.1根据用户选择的支付方式来判断订单初始状态
        status = (OrderInfo.ORDER_STATUS_ENUM['UNPAID']
                  if pay_method == OrderInfo.PAY_METHODS_ENUM['ALIPAY']
                  else OrderInfo.ORDER_STATUS_ENUM['UNSEND'])
        #3.2拼接order_id      当前时间+用户id
        order_id =  timezone.now().strftime('%Y%m%d%H%M%S') + '%09d' %user.id
        #涉及到四个表的处理，提交订单新增一条订单信息记录，新增多条订单商品记录，修改sku、spu表记录
        #3.3新增订单信息记录  total_count   total_amount暂时还未得到数据
        #这四张表的操作应该具有一致性，要么全部都成功，要么全部都不成功，所以应该添加事务
        with transaction.atomic():

            save_point1 = transaction.savepoint() #创建保存点
            try: #保证操作的有效性，不管中间发生任何异常统统回滚
                order_model = OrderInfo.objects.create(
                    order_id=order_id,
                    user=user,
                    address=address,
                    total_count=0,
                    total_amount=Decimal('0.00'),
                    freight=Decimal('10.00'),
                    pay_method=pay_method,
                    status=status,
                )
                #3.4新增多条订单商品记录
                #要想获取订单商品信息，需要去redis查询该用户勾选状态下的商品
                redis_conn = get_redis_connection('carts')
                redis_carts = redis_conn.hgetall('cart_user%s' %user.id)
                redis_selected = redis_conn.smembers('selected_user%s' %user.id)
                for sku_id_byte in redis_selected:
                    while True:
                        buy_count = int(redis_carts[sku_id_byte])         #购买量
                        sku_id = int(sku_id_byte)
                        #查询商品对应的sku模型表
                        try:
                            sku = SKU.objects.get(id=sku_id,is_launched=True)     #sku模型
                        except SKU.DoesNotExist:
                            return HttpResponseForbidden('商品不存在')
                        #查询商品库存数量，是否能够满足需求
                        origin_stock = sku.stock    #原库存量
                        origin_sales = sku.sales    #原销量
                        if buy_count > origin_stock:
                            # transaction.rollback()
                            transaction.savepoint_rollback(save_point1)  #回滚到保存点
                            return JsonResponse({'code':RETCODE.STOCKERR,'errmsg':'库存不足'})
                        #如果库存充足，则修改sku的库存数、销量以及spu销量
                        # sku.stock -= buy_count
                        # sku.sales += buy_count
                        # sku.save()              #修改sku表并保存
                        time.sleep(5)
                        new_stock = origin_stock - buy_count
                        new_sales = origin_sales + buy_count
                        #使用乐观锁
                        result = SKU.objects.filter(id=sku_id,stock=origin_stock).update(stock=new_stock,sales=new_sales)
                        if result == 0:  #说明没有修改成功，原数据已被他人更改
                            continue
                        spu = sku.spu      #spu模型
                        spu.sales += buy_count
                        spu.save()          #修改spu表并保存
                        #新增多条订单商品记录
                        OrderGoods.objects.create(
                            order_id=order_id,
                            sku=sku,
                            count=buy_count,
                            price=sku.price,
                        )
                        #对订单信息中的总数量和总金额进行累加
                        order_model.total_count += buy_count
                        order_model.total_amount += (buy_count*sku.price)
                        # 下单成功或者失败就跳出循环
                        break
                    #最后总金额还需要算上运费，保存订单记录
                    order_model.total_amount += order_model.freight
                    order_model.save()

            except Exception as e:
                logger = logging.getLogger('django')
                logger.info(e)
                transaction.savepoint_rollback(save_point1)  #回滚到保存点
                return JsonResponse({'code': RETCODE.DBERR, 'errmsg': '下单失败'})
            else:
                # transaction.commit()
                transaction.savepoint_commit(save_point1)  #提交保存点
        #3.3删除购物车中已经购买过的商品，即删除redis中的相关商品信息
        # for sku_id_byte in redis_selected:
        #     redis_conn.hdel('cart_user%s' %user.id,sku_id_byte)
        redis_conn.delete('selected_user%s' %user.id)
        redis_conn.hdel('cart_user%s' %user.id,*redis_selected)      #将set集合作为key进行一次删除
        #4.拼接order_id，响应json数据
        return JsonResponse({'code':RETCODE.OK,'errmsg':'ok','order_id':order_id})


class OrderSuccessView(LoginRequireView):
    """订单提交成功界面"""
    def get(self, request):
        #1.获取数据
        #order_id=20191201124007000000002&payment_amount=6509&pay_method=2
        order_id = request.GET.get('order_id')
        payment_amount = request.GET.get('payment_amount')
        pay_method = request.GET.get('pay_method')
        #2.校验数据
        #查看订单信息表，是否存在该条订单信息
        try:
            OrderInfo.objects.get(order_id=order_id,total_amount=payment_amount,pay_method=pay_method,user=request.user)
        except OrderInfo.DoesNotExist:
            return HttpResponseForbidden('请求信息有误')
        #3.提供数据
        #4.渲染页面
        context = {
            'payment_amount':payment_amount,
            'order_id':order_id,
        }
        return render(request,'order_success.html',context)


class OrderInfoView(LoginRequireView):
    """用户中心全部订单展示"""
    def get(self,request,page_num):
        #1.接收数据
        #2.校验数据
        #3.业务逻辑
        user = request.user   #获取当前登录用户
        orders_qs = OrderInfo.objects.filter(user=user).order_by('-create_time')
        for order in orders_qs:
            order.pay_method_name = OrderInfo.PAY_METHOD_CHOICES[order.pay_method-1][1]
            order.status_name = OrderInfo.ORDER_STATUS_CHOICES[order.status-1][1]
            order_goos_qs = OrderGoods.objects.filter(order_id=order.order_id)   #查询该用户不同订单下的所有订单商品
            for goods in order_goos_qs:
                sku = SKU.objects.get(id=goods.sku_id)
                goods.default_image_url = sku.default_image.url
                goods.amount = (goods.price) * (goods.count)
                goods.name = sku.name
            order.sku_list = order_goos_qs
        page = 5
        page_num = int(page_num)
        #创建分页器对象
        paginator = Paginator(orders_qs,page)
        #将需要分页的对象和每页的个数传进去
        page_orders = paginator.page(page_num)  #按页数获取分页后每页的数据
        total_page = paginator.num_pages   #调用该property方法得到总页数，这个方法用属性方式表示

        #4.渲染页面
        context = {
            'page_orders':page_orders,
            'page_num':page_num,
            'total_page':total_page,
        }
        return render(request,'user_center_order.html',context)


class OrderCommentView(LoginRequireView):
    """订单评价"""
    def get(self,request):
        #orders/comment/?order_id=20191201122939000000002
        #1.接收数据
        order_id = request.GET.get('order_id')
        #2.校验数据
        try:
            order = OrderInfo.objects.get(order_id=order_id,status=OrderInfo.ORDER_STATUS_ENUM['UNCOMMENT'])
        except OrderInfo.DoesNotExist:
            return HttpResponseForbidden('无效订单')
        #3.业务逻辑处理
        #通过订单查询多个订单商品，将其包装成字典格式
        #这个地方需要过滤掉已经评论过的订单商品
        ordergoods_qs = order.skus.filter(is_commented=False)
        uncomment_goods_list = []
        for ordergoods in ordergoods_qs:
            sku = ordergoods.sku
            sku_dict = {
                'order_id':order_id,
                'sku_id':sku.id,
                'name':sku.name,
                'price':str(sku.price),
                'sku.default_image_url':sku.default_image.url,
            }
            uncomment_goods_list.append(sku_dict)
        #4.渲染页面
        context = {
            'uncomment_goods_list':uncomment_goods_list,
        }
        return render(request,'goods_judge.html',context)

    def post(self,request):
        """提交商品评价"""
        #1.接收数据
        #{'sku_id': 4, 'comment': '很好，非常满意', 'score': 5, 'is_anonymous': True}
        json_dict = json.loads(request.body.decode())
        order_id = json_dict.get('order_id')
        sku_id = json_dict.get('sku_id')
        comment = json_dict.get('comment')
        score = json_dict.get('score')
        is_anonymous = json_dict.get('is_anonymous')
        #2.校验数据
        try:
            order = OrderInfo.objects.get(order_id=order_id,status=OrderInfo.ORDER_STATUS_ENUM['UNCOMMENT'])
        except OrderInfo.DoesNotExist:
            return HttpResponseForbidden('无效订单')
        try:
            ordergoods = order.skus.get(sku_id=sku_id,is_commented=False)
            sku = ordergoods.sku
        except OrderGoods.DoesNotExist:
            return HttpResponseForbidden('无效订单商品')
        if len(comment) <= 5:
            return HttpResponseForbidden('无效评论')
        #3.将评价同步到商品详情页面
        #向订单商品表中的评论字段添加信息,并将is_comment字段改成True
        ordergoods.comment = comment
        ordergoods.is_commented = True
        ordergoods.save()
        #每次评论完之后判断一下，所有订单商品有没有评论，如果已经评论，将订单状态改为已完成
        # for order_goods in order.skus.all():
        #     if order_goods.is_commented == False:
        #         break
        # else:
        #     order.status = OrderInfo.ORDER_STATUS_ENUM['FINISHED']
        #     order.save()
        #这里只需要每次判断一下未平论商品个数
        if order.skus.filter(is_commented=False).count() == 0:
            order.status = OrderInfo.ORDER_STATUS_ENUM['FINISHED']
            order.save()
        #4.响应json
        return JsonResponse({'code':RETCODE.OK,'errmsg':'ok'})

