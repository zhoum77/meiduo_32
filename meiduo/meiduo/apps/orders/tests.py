from django.test import TestCase

# Create your tests here.
from datetime import datetime,timezone,timedelta
# cur_time = datetime.utcnow()
cur_time = datetime.now(tz=timezone(timedelta(hours=0)))
print('cur_time:',cur_time)
cur_time_0 = cur_time.replace(tzinfo=timezone(timedelta(hours=0)))
print('cur_time_0:',cur_time_0)
cur_time_sh = cur_time_0.astimezone(tz=timezone(timedelta(hours=8)))
print('cur_time_sh:',cur_time_sh)