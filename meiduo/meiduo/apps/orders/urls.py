from django.conf.urls import url
from .views import OrderSettlementView,OrderCommitView,OrderSuccessView,OrderInfoView,OrderCommentView
urlpatterns = [
    #结算订单
    url(r'^orders/settlement/$',OrderSettlementView.as_view()),
    #提交订单
    url(r'^orders/commit/$',OrderCommitView.as_view()),
   #提交订单后成功的页面
    url(r'^orders/success/$',OrderSuccessView.as_view()),
    #用户中心全部订单展示
    url(r'^orders/info/(?P<page_num>\d+)/$',OrderInfoView.as_view()),
    #订单评价页面
    url(r'^orders/comment/$',OrderCommentView.as_view()),
]
