from django.shortcuts import render
from django.views import View
from django.http import JsonResponse,HttpResponseForbidden
from django.core.cache import cache
from .models import Area
from meiduo.utils.response_code import RETCODE

class AreasViews(View):
    """获取省市区信息"""
    def get(self,request):
        #url = this.host + '/areas/?area_id=' + this.form_address.city_id;
        #1.接收数据
        id = request.GET.get("area_id")
        #2.判断是否有查询参数，没有查询参数说明是获取省份数据，有查询参数是查询市区信息
        #采用mysql磁盘型数据库存储数据，将来查找的次数过多会大大降低mysql的性能
        #对于获取省份数据而言，每个用户都要获取，可以在第一次获取时将其加载到redis内存中，这样就不用每次都操作mysql数据库了
        # 3.业务逻辑处理
        if id is None:
            #获取省份数据
            #3.0 先判断redis中有没有省份数据，如果有直接从redis读取，如果没有就从mysql读取，读完后存到redis中
            #借助django封装的方法可以解决
            province_list = cache.get('province_list')
            #如果能从redis中获取数据，则直接响应
            if province_list is None:
            #3.1获取所有的省级信息

                province_qs = Area.objects.filter(parent=None)
                #3.2 获得的是queryset类型，不能直接传给json数据，需要将其包装成字典或者列表
                province_list = []
                for province in province_qs:
                    province_dict = {'id':province.id,'name':province.name}
                    province_list.append(province_dict)
                #将获取到的数据存入redis
                cache.set("province_list",province_list,3600)
            # 4.响应Json数据
            return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK','province_list':province_list})

        else:
            #查询市区信息 id = request.GET.get("area_id")
            #1.根据获取的省级id信息，查询下面所有的市
            #市区信息也可以用redis存储
            #先查看redis中有没有，没有就用mysql查，有就直接用redis缓存的数据
            sub_data = cache.get("sub_data_%s" %id)
            if sub_data is None:
                try:
                    area = Area.objects.get(id=id)  #获取模型对象
                    city_qs = area.subs.all()  #得到qs集合
                # "sub_data" = {
                #     "id"=id,
                #     "name"=name,
                #     "subs"=[
                #         {'id':id,'name':name}
                #     ]
                # }
                except Area.DoesNotExist:
                    return HttpResponseForbidden("缺少id值")

                # 2.将area和city_qs数据以json形式响应
                subs = []
                for city in city_qs:
                    subs_dict = {'id': city.id, 'name': city.name}
                    subs.append(subs_dict)
                sub_data = {'id': area.id, 'name': area.name, "subs": subs}
                cache.set("sub_data_%s" %id,sub_data,3600)
            return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'sub_data': sub_data})



