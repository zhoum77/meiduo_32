from django.conf.urls import url
from .views import AreasViews

urlpatterns = [
    #查询省市区信息
    url(r'^areas/$',AreasViews.as_view()),
]