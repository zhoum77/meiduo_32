from django.shortcuts import render
from django.http import JsonResponse,HttpResponseForbidden
from django.conf import settings
from alipay import AliPay
import os

from meiduo.utils.views import LoginRequireView
from meiduo.utils.response_code import RETCODE
from orders.models import OrderInfo
from .models import Payment
# Create your views here.
class PaymentURLView(LoginRequireView):
    """拼接支付链接"""
    def get(self,request,order_id):
        #1.接收数据
        #2.校验数据
        try:
            #检验该订单id是否存在且为该用户未支付的订单
            order = OrderInfo.objects.get(order_id=order_id,user=request.user,status=OrderInfo.ORDER_STATUS_ENUM['UNPAID'])
        except OrderInfo.DoesNotExist:
            return HttpResponseForbidden('无效订单')
        #3.借助sdk第三方包拼接支付链接
        #3.1创建AliPay对象
        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),'keys/app_private_key.pem'),
            # 绝对路径拼接,
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),'keys/alipay_public_key.pem'),
            sign_type="RSA2", # RSA 或者 RSA2
            debug = settings.ALIPAY_DEBUG  # 默认False
            )
        #3.2调用api_alipay_trade_page_pay方法
        order_string = alipay.api_alipay_trade_page_pay(
            out_trade_no=order_id,  # 要支付的美多订单编号
            total_amount=str(order.total_amount),  # 支付金额,注意转换类型
            subject='美多商城:%s' % order_id,
            return_url=settings.ALIPAY_RETURN_URL,  # 回调url
        )
        # 3.3. 拼接支付登录url
        # 真实支付宝登录url: https://openapi.alipay.com/gateway.do? + order_string
        # 沙箱支付宝登录url: https://openapi.alipaydev.com/gateway.do? + order_string
        #4.响应json数据 alipay_url
        alipay_url = settings.ALIPAY_URL + '?' + order_string
        return JsonResponse({'code':RETCODE.OK,'errmsg':'ok','alipay_url':alipay_url})

class PaymentStatusView(LoginRequireView):
    """保存支付结果"""
    def get(self,request):
        #1.接收数据
        data = request.GET.dict()
        sign = data.pop('sign')
        #2.校验数据
        #创建alipay对象，调用verify方法校验data 和sign
        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/app_private_key.pem'),
            # 绝对路径拼接,
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                'keys/alipay_public_key.pem'),
            sign_type="RSA2",  # RSA 或者 RSA2
            debug=settings.ALIPAY_DEBUG  # 默认False
        )
        #如果data和sign匹配成功，则返回True
        if not alipay.verify(data,sign):
            return HttpResponseForbidden('支付失败')
        #3.创建支付模型类，保存支付数据
        order_id = data.get('out_trade_no')
        trade_id = data.get('trade_no')
        try:
            #查看该订单是否存在，且为该用户的订单
            order = OrderInfo.objects.get(order_id=order_id,user=request.user)
        except OrderInfo.DoesNotExist:
            return HttpResponseForbidden('无效订单')
        else:
            #检查该订单是否已被支付
            if order.status == OrderInfo.ORDER_STATUS_ENUM['UNPAID']:
                payment = Payment.objects.create(
                    order=order,    #订单号
                    trade_id=trade_id,    #交易号
                )
                #4.将订单信息表中订单的状态改为代发货
                order.status = OrderInfo.ORDER_STATUS_ENUM['UNCOMMENT']
                order.save()
        #渲染支付成功页面

        context = {
            'trade_id':data['out_trade_no'],
        }
        return render(request,'pay_success.html',context)
#payment/status/
# ?charset=utf-8
# &out_trade_no=20191201130015000000002
# &method=alipay.trade.page.pay.return
# &total_amount=6509.00
# &sign=Tyr0TU6Sv%2Bqpw6H52PlKpHvQNkH7jiXIzVcoXGvr1iQMUmz6vIFtznSx1GOJqLO5H3npSvbqWapAaiCdNt1XYIcEK2zboTHhBy998F9ZMxv59mvlKanZ%2BO7UXK%2FwdLSCKJM6jBCQUYTb72ncfMTLVGGT29GaoXACmaXC6z41cNa2nR07PP7dgeJpYq%2BmKT2X2u1XmiHdmm7AL3uiHROxPamXjKMBZmCjeKjI6Q7wIh59mBBHOP3DdhxxPpVtwO3flAV3xdRVsg%2B7Oj%2BCyZ8kdciVRpyZKG9GMq4P5LjVaIeZKyJ7zXqNeULgWnMN57PTTCuZ1v5GBxGqRwPCMaKRQQ%3D%3D&trade_no=2019120122001422971000030041&auth_app_id=2016101700704941&version=1.0&app_id=2016101700704941&sign_type=RSA2&seller_id=2088102179930172&timestamp=2019-12-01+23%3A25%3A51 HTTP/1.1" 404 8351
#{'charset': 'utf-8',
# 'out_trade_no': '20191201130015000000002',
# 'method': 'alipay.trade.page.pay.return',
# 'total_amount': '6509.00',
# 'trade_no': '2019120122001422971000030041',
# 'auth_app_id': '2016101700704941',
# 'version': '1.0',
# 'app_id': '2016101700704941',
# 'sign_type': 'RSA2',
# 'seller_id': '2088102179930172',
# 'timestamp': '2019-12-01 23:25:51'}