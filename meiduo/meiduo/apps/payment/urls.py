from django.conf.urls import url
from .views import PaymentURLView,PaymentStatusView
urlpatterns = [
    #支付请求页面
    url(r'^payment/(?P<order_id>\d+)/$',PaymentURLView.as_view()),
    #支付成功回调的页面
    url(r'^payment/status/$',PaymentStatusView.as_view()),
]