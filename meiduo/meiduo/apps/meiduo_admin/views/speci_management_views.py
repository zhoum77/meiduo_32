from rest_framework.viewsets import ModelViewSet
from goods.models import SPUSpecification,SpecificationOption
from meiduo_admin.serializers.sku_management_serializer import SPUSpecSerializer,SpecOptionSerializer
from meiduo_admin.defined_pagination import MyPage
class SpecManagementViewSet(ModelViewSet):
    """规格表管理"""
    #实现对规格表的增删改查
    queryset = SPUSpecification.objects.all()
    serializer_class = SPUSpecSerializer
    pagination_class = MyPage


class OptionManagementViewSet(ModelViewSet):
    """规格选项管理"""
    queryset = SpecificationOption.objects.all()
    serializer_class = SpecOptionSerializer
    pagination_class = MyPage

#AssertionError: The field 'spec_id' was declared on serializer SpecOptionSerializer, but has not been included in the 'fields' option.
