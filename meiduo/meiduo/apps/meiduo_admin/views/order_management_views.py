from rest_framework.viewsets import ModelViewSet
from orders.models import OrderInfo
from meiduo_admin.defined_pagination import MyPage
from meiduo_admin.serializers.order_management_serializer import OrderManagementSerializer,OrderDetailSerializer

class OrderManagementViewSet(ModelViewSet):
    queryset = OrderInfo.objects.all()
    serializer_class = OrderManagementSerializer
    pagination_class = MyPage


    #重写get_queryset方法对keyword过滤
    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword:
            return self.queryset.filter(order_id__contains=keyword)
        else:
            return self.queryset.all()

    #获取某一订单详细信息时，发现，需要展示的数据比原序列化返回的信息要多很多
    #所以，这个地方获取单一数据，需要使用一个更加详细的序列化器进行处理，并对get_serializer_class方法进行重写
    def get_serializer_class(self):
        if self.action == 'retrieve':
            return OrderDetailSerializer
        else:
            return self.serializer_class


