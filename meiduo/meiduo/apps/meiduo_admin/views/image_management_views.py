from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from goods.models import SKUImage,SKU
from meiduo_admin.serializers.image_management_serializer import ImageManagementSerializer
from meiduo_admin.defined_pagination import MyPage
from meiduo_admin.serializers.sku_management_serializer import SKUManagementSerializer
class ImageManagementViewSet(ModelViewSet):
    """图片表管理"""
    queryset = SKUImage.objects.all()
    serializer_class = ImageManagementSerializer
    pagination_class = MyPage

class SKUShowView(ListAPIView):
    """在新增图片视图中，添加sku展示视图"""
    queryset = SKU.objects.all()
    serializer_class = SKUManagementSerializer


