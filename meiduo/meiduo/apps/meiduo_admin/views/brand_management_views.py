from rest_framework.viewsets import ModelViewSet
from goods.models import Brand
from meiduo_admin.serializers.brand_management_serializer import *
from meiduo_admin.defined_pagination import MyPage
class BrandManagementViewSet(ModelViewSet):
    """品牌管理视图"""
    queryset = Brand.objects.all()
    serializer_class = BrandManagementSerializer
    pagination_class = MyPage