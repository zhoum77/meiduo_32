from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from goods.models import SKU,SPU,SPUSpecification
from meiduo_admin.serializers.sku_management_serializer import SKUManagementSerializer,CategorySerializer,SPUSerializer,SPUSpecSerializer
from meiduo_admin.defined_pagination import MyPage
from goods.models import GoodsCategory
class SKUManagementView(ModelViewSet):
    """sku表管理"""
    #get 获取、查询sku商品
    #post 新增一条sku商品信息
    queryset = SKU.objects.all()
    serializer_class = SKUManagementSerializer
    pagination_class = MyPage

    def get_queryset(self):
        #获取查询参数keyword
        keyword = self.request.query_params.get('keyword')
        #过滤
        if keyword:
            return self.queryset.filter(name__contains=keyword).order_by('id')
        else:
            return self.queryset.all().order_by('id')


class CategoryShowView(ListAPIView):
    """获取三级分类信息"""
    queryset = GoodsCategory.objects.filter(id__gte=38)
    serializer_class = CategorySerializer

    # def get_queryset(self):
    #     #过滤得到三级分类商品信息
    #     return self.queryset.filter(id__gte=38)

class SPUShowView(ListAPIView):
    """获取spu表名称数据"""
    queryset = SPU.objects.all()
    serializer_class = SPUSerializer

class SPUSpecShowView(ListAPIView):
    """获取SPU商品规格信息"""
    queryset = SPUSpecification.objects.all()
    serializer_class = SPUSpecSerializer

    def get_queryset(self):
        #1.获取路径正则匹配数据
        spu_id = self.kwargs.get('pk')
        #2.过滤
        return self.queryset.filter(spu_id=spu_id)

class SKUModifyDelView(ModelViewSet):
    """sku表管理"""
    #get 获取某一sku商品
    #push 更新一条sku商品信息
    #delete 删除一条sku商品
    queryset = SKU.objects.all()
    serializer_class = SKUManagementSerializer


