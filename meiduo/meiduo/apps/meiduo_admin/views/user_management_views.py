from rest_framework.generics import ListCreateAPIView,ListAPIView
from users.models import User
from meiduo_admin.serializers.user_management_serializer import UserManagementSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from meiduo_admin.defined_pagination import MyPage
class UserListCreateView(ListCreateAPIView):
    """用户管理：展示用户信息，新增用户"""
    queryset = User.objects.all()
    serializer_class = UserManagementSerializer
    pagination_class = MyPage
    #重写get_queryset()方法，对keyword进行过滤
    def get_queryset(self):
        #1.获取request对象中查询参数中的keyword信息
        keyword = self.request.query_params.get('keyword')
        if keyword:
            return self.queryset.filter(username__contains=keyword,is_staff=True).order_by('id')
        else:
            return self.queryset.filter(is_staff=True).order_by('id')