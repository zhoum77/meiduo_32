from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from rest_framework.decorators import action
from rest_framework.response import Response
from meiduo_admin.serializers.sku_management_serializer import CategorySerializer
from goods.models import GoodsChannel,GoodsChannelGroup,GoodsCategory
from meiduo_admin.serializers.channel_management_serializer import ChannelManagementSerializer,ChannelGroupSerializer
from meiduo_admin.defined_pagination import MyPage
class ChannelManagementViewSet(ModelViewSet):
    """频道管理视图"""
    queryset = GoodsChannel.objects.all()
    serializer_class = ChannelManagementSerializer
    pagination_class = MyPage
    #重写get_queryset和get_serializer_class方法
    def get_queryset(self):
        if self.action == 'get_channelgroup':
            return GoodsChannelGroup.objects.all()
        else:
            return self.queryset.all()

    def get_serializer_class(self):
        if self.action == 'get_channelgroup':
            return ChannelGroupSerializer
        else:
            return self.serializer_class

    #获取频道组信息
    @action(['get'],detail=False)
    def get_channelgroup(self,request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset,many=True)
        return Response(serializer.data)

class GoodsCategoriesView(ListAPIView):
    """新增频道中获取一类商品分类信息"""
    queryset = GoodsCategory.objects.all()
    serializer_class = CategorySerializer
    #过滤得到一级分类信息
    def get_queryset(self):
        return self.queryset.filter(parent_id__lt=38)

