from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from users.models import User
from goods.models import GoodsVisitCount
from meiduo_admin.serializers.category_visit_serializer import CategoryVisitSerializer
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from datetime import date,timedelta,datetime,timezone
from django.utils import timezone as dj_timezone
from django.db.models.query import QuerySet
import pytz
from django.conf import settings
class UserCountView(ViewSet):
    """用户总数统计"""
    @action(['get'],detail=False)
    def total_count(self,request):
        #1.获取当前时间
        cur_datetime_utc = datetime.utcnow()   #得到的是没有时区标识的零时区时间
        cur_datetime_0 = cur_datetime_utc.replace(tzinfo=timezone(timedelta(hours=0)))  #添加时区属性
        #基于本地业务逻辑处理的需要，需要将UTC(0时区)的时间换算成动八区的时间（带时区）
        #将动八区带时区的时间和数据库utc时间进行比较时，系统会自动进行时区换算
        cur_datetime_sh = cur_datetime_0.astimezone(tz=timezone(timedelta(hours=8)))
        cur_datetime_sh_0 = cur_datetime_sh.replace(hour=0,minute=0,second=0)

        #2.查询用户表信息
        count = User.objects.all().count()
#cur_datetime_0 = {datetime} 2019-12-11 10:03:37.056185+00:00
# cur_datetime_sh = {datetime} 2019-12-11 18:03:37.056185+08:00
# cur_datetime_utc = {datetime} 2019-12-11 10:03:37.056185
#cur_datetime_sh_0=2019-12-11 00:00:00.956137+08:00
        #3.返回数据
        return Response({'count':count,'date':cur_datetime_sh.date()})

    #"""日增用户统计"""
    @action(['get'], detail=False)
    def day_increment(self,request):
        #1.获取本地零时时间
        dj_datetime = dj_timezone.now()
        #2019-12-11 10:20:23.926256+00:00   django自带的timezone.now()得到的是UTC零时区带时区的时间
        # datetime_now = datetime.now()
        #2019-12-11 18:23:09.607389  python 中datetime.now()得到的是本地不带时区的时间
        #1）直接通过时区数进行时区转换
        # local_datetime = dj_datetime.astimezone(tz=timezone(timedelta(hours=8)))
        #dj_datetime = {datetime} 2019-12-11 10:26:36.161371+00:00
        # local_datetime = {datetime} 2019-12-11 18:26:36.161371+08:00

        #2）利用pytz快速构建时区对象，进行转换   只需要在timezone()传入时区名

        # local_datetime = dj_datetime.astimezone(tz=pytz.timezone('Asia/Shanghai'))

        local_datetime = dj_datetime.astimezone(tz=pytz.timezone(settings.TIME_ZONE))
        local_datetime_0 = local_datetime.replace(hour=0,minute=0,second=0,microsecond=0)

        #2.查询用户信息
        now_date = local_datetime_0.date()
        count = User.objects.filter(date_joined__gte=local_datetime_0).count()   #注意时间的比较

        #3.返回json
        return Response({'count':count,'date':now_date})

    #"""日活用户统计"""
    @action(['get'], detail=False)
    def day_active(self,request):
        # 1.获取本地时间零时刻
        local_time = datetime.utcnow()
        local_datetime_0 = local_time.replace(hour=0,minute=0,second=0,microsecond=0,tzinfo=timezone(timedelta(hours=8)))
        # # 2.查询用户信息
        # now_date = local_datetime_0.date()
        # now_date = datetime.today()
        count = User.objects.filter(last_login__gte=local_datetime_0).count()
        #3.返回数据
        return Response({'count':count,'date':local_datetime_0.date()})



    #"""日下单用户量统计"""
    @action(['get'], detail=False)
    def day_orders(self,request):
        # 1.获取本地时间零时刻
        local_time = datetime.utcnow()
        local_datetime_0 = local_time.replace(hour=0,minute=0,second=0,microsecond=0,tzinfo=timezone(timedelta(hours=8)))

        #关联过滤查询
        user_qs = User.objects.filter(orderinfo__create_time__gte=local_datetime_0)
        #创建set集合过滤重复用户
        user_set = set()
        for user in user_qs:
            user_set.add(user)
        #3.返回数据
        return Response({'count':len(user_set),'date':local_datetime_0.date()})



    #"""月增用户统计"""
    # def get(self,request):   #本月新增用户统计
    #     # 1.获取token数据
    #     # 2.查询用户订单信息
    #     now_date = date.today()   #2019-12-10
    #     now_month = now_date - timedelta(days=now_date.day-1)  #2019-12-01
    #     user_month_incre_list = []
    #     for i in range(int(now_date.day)):
    #         start = now_month + timedelta(days=i)
    #         end = now_month + timedelta(days=i+1)
    #         count = User.objects.filter(date_joined__gte=start,date_joined__lt=end).count()
    #         user_month_incre_list.append({
    #             'count':count,
    #             'date':start,
    #         })
    @action(['get'], detail=False)
    def month_increment(self,request):     #从当天算起，前30天新增用户统计
        #1.获取当前时间
        utc_datetime = datetime.utcnow()
        utc_datetime_tz = utc_datetime.replace(tzinfo=timezone(timedelta(hours=0)))
        local_datetime_tz = utc_datetime_tz.astimezone(tz=pytz.timezone(settings.TIME_ZONE))
        local_datetime_0 = local_datetime_tz.replace(hour=0,minute=0,second=0,microsecond=0)
        month_datetime_0 = local_datetime_0 - timedelta(days=29)
        user_month_incre_list = []
        for index in range(30):
            start_time = month_datetime_0 +timedelta(days=index)
            end_time = start_time + timedelta(days=1)
            count = User.objects.filter(date_joined__gte=start_time,date_joined__lt=end_time).count()
            user_month_incre_list.append({'count':count,'date':start_time.date()})
        return Response(user_month_incre_list)


# class CategoryDateVisitView(APIView):
#     """日分类商品访问量"""
#     def get(self,request):
#         #1.查看商品访问表
#         now_date = date.today()
#         category_date_visit_list = []
#         category_qs = GoodsVisitCount.objects.filter(date__gte=now_date)
#         for category in category_qs:
#             category_dict = {'category':category.category.name,'count':category.count}
#             category_date_visit_list.append(category_dict)
#         return Response(category_date_visit_list)
class CategoryDateVisitView(ListAPIView):
    """日分类商品访问量"""

    queryset = GoodsVisitCount.objects.all()  #在django工程开启后，类属性只会运行一次
    # queryset = GoodsVisitCount.objects.filter(date__gte=local_datetime_0)  #在django工程开启后，类属性只会运行一次
    serializer_class = CategoryVisitSerializer
    #当django工程开启后，类属性只会在开启的时候执行一次，得到的queryset是最开始的值，
    #通过调用get_queryset方法，再次查询queryset，保证queryset的准确性
    #但是这里需要过滤，不能直接调用get_queryset，需要将其重写
    def get_queryset(self):
        # 获取本地零时刻
        utc_datetime_tz = dj_timezone.now()  # 得到当前时刻utc时区的时间
        local_datetime_tz = utc_datetime_tz.astimezone(tz=pytz.timezone(settings.TIME_ZONE))  # 得到当前时刻带时区的本地时间
        local_datetime_0 = local_datetime_tz.replace(hour=0, minute=0, second=0, microsecond=0)
        queryset = self.queryset
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            queryset = queryset.filter(date__gte=local_datetime_0)
        return queryset
