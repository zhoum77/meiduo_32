from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Permission,ContentType
from meiduo_admin.serializers.permission_management_serializer import PermissionSerializer,ContentTypeSerializer
from meiduo_admin.defined_pagination import MyPage
class PermissionViewSet(ModelViewSet):
    """权限管理视图"""
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    pagination_class = MyPage

    def get_queryset(self):
        return self.queryset.order_by('id')



class ContentTypeView(ListAPIView):
    """权限类型"""
    queryset = ContentType.objects.all()
    serializer_class = ContentTypeSerializer
