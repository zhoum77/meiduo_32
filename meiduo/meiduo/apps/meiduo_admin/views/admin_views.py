from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from users.models import User
from django.contrib.auth.models import Group
from meiduo_admin.serializers.admin_serializer import *
from meiduo_admin.defined_pagination import MyPage
from meiduo_admin.serializers.user_group_management_serializer import UserGroupManagementSerializer
from rest_framework.permissions import BasePermission,IsAdminUser
#按照IsAuthenticated权限认证的方式，自定义权限认证
class IsEat(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.has_perm('admin.can eat'))


class AdminViewSet(ModelViewSet):
    """管理员视图"""
    queryset = User.objects.filter(is_staff=True)
    serializer_class = AdminSerializer
    pagination_class = MyPage
    permission_classes = [IsEat]

class GroupsView(ListAPIView):
    """获取用户组信息"""
    queryset = Group.objects.all()
    serializer_class = UserGroupManagementSerializer


