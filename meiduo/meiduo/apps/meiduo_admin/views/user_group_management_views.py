from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import Group,Permission
from meiduo_admin.serializers.user_group_management_serializer import *
from meiduo_admin.defined_pagination import MyPage
from meiduo_admin.serializers.permission_management_serializer import PermissionSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
class UserGroupManagementViewSet(ModelViewSet):
    """用户管理视图"""
    queryset = Group.objects.all()
    serializer_class = UserGroupManagementSerializer
    pagination_class = MyPage

    def get_queryset(self):
        if self.action == 'get_permission':
            return Permission.objects.all()
        else:
            return self.queryset


    def get_serializer_class(self):
        if self.action == 'get_permission':
            return PermissionSerializer
        else:
            return self.serializer_class

    @action(['get'],detail=False)
    def get_permission(self,request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset,many=True)
        return Response(serializer.data)

