from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from meiduo_admin.serializers.login_serializer import LoginSerializer
from django.http import HttpResponseForbidden
# Create your views here.

class LoginView(APIView):
    """登录验证"""
    def post(self,request):
        #1.接收前端数据
        #2.创建序列化对象
        serializer = LoginSerializer(data=request.data)
        #3.验证  并内部自动抛出异常
        if serializer.is_valid(raise_exception=True):
            return Response({
                'username':serializer.validated_data['user'].username,
                'user_id':serializer.validated_data['user'].id,
                'token':serializer.validated_data['token']
            })

        #获得有效数据，并构建响应参数
