from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView,ListAPIView
from rest_framework.response import Response
from rest_framework.decorators import action
from goods.models import SPU,Brand,GoodsCategory
from meiduo_admin.serializers.spu_management_serializer import *
from meiduo_admin.serializers.sku_management_serializer import CategorySerializer
from meiduo_admin.defined_pagination import MyPage
class SPUManagementViewSet(ModelViewSet):
    """SPU商品管理"""
    queryset = SPU.objects.all()
    serializer_class = SPUSerializer
    pagination_class = MyPage
    #重写get_queryset方法，返回指定的模型集
    def get_queryset(self):
        #根据不同的视图请求返回指定模型集
        if self.action == 'get_brand':
            return Brand.objects.all()
        else:
            return self.queryset.all()
    #重写get_serializer_class方法实现指定序列化器
    def get_serializer_class(self):
        #还是通过请求视图函数，来判断指定哪个序列化器
        if self.action == 'get_brand':
            return BrandSerializer
        else:
            return self.serializer_class
#1）可以通过直接定义一个视图来完成品牌展示
# class BrandShowView(ModelViewSet):
#     """品牌展示"""
#     queryset = Brand.objects.all()
#     serializer_class = BrandSerializer
#2）因为品牌展示是spu商品新增时，需要展示的，可以直接定义在spu商品管理视图集中
    @action(['get'],detail=False,url_path='brands/simple',url_name='brandshow')
    def get_brand(self,request):
        """品牌展示"""
        # #1.获取需要查询的品牌模型集
        # brand_qs = Brand.objects.all()
        # #2.创建序列化器对象
        # bs = BrandSerializer(brand_qs,many=True)
        # #3.序列化返回
        # return Response(bs.data)
        queryset = self.get_queryset()   #得到的queryset是视图集中类属性定义的spu表模型集
        serializer = self.get_serializer(queryset,many=True)  #序列化器也是spu的序列化器，queryset和serializer都需要重写
        return Response(serializer.data)



class CategoryOneView(ListAPIView):
    """展示一、二、三级商品分类信息"""
    queryset = GoodsCategory.objects.all()
    serializer_class = CategorySerializer

    #重写get_queryset方法，进行过滤
    def get_queryset(self):
        parent_id = self.kwargs.get('pk')
        if parent_id:
            return self.queryset.filter(parent_id=parent_id)
        else:
            return self.queryset.filter(parent_id=None)

