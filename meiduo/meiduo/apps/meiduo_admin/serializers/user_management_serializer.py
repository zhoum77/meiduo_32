from rest_framework import serializers
from users.models import User
from django.contrib.auth.hashers import make_password
class UserManagementSerializer(serializers.ModelSerializer):
    """定义用户模型类序列化器"""
    class Meta:
        model = User
        fields = [
            "id",  #默认主键只用来做序列化 read_only = True
            "username",
            "mobile",
            "email",
            'password',
        ]
        extra_kwargs = {
            'password':{'write_only':'True'},
        }

    #1)通过重写validate方法，实现密码密文和添加is_staff=True有效数据
    # def validate(self, attrs):
    #     #1.将密码设置为密文
    #     password = attrs.get('password')
    #     password = make_password(password)
    #     attrs['password'] = password
    #     #2.额外添加is_staff=True有效数据
    #     attrs['is_staff'] = True
    #     return attrs

    #2）通过重写模型对象创建方法，实现
    def create(self, validated_data):
        return self.Meta.model.objects.create_superuser(**validated_data)

