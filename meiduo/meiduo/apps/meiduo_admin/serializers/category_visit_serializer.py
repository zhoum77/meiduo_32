from rest_framework import serializers
from goods.models import GoodsVisitCount
class CategoryVisitSerializer(serializers.ModelSerializer):
    """自定义模型类序列化器"""
    #因为要得到的数据是"category": "分类名称", 使用模型类直接映射得到的只是外键id
    # category = serializers.PrimaryKeyRelatedField(read_only=True)
    category = serializers.StringRelatedField()   #使用字符串关联外键，得到的是str魔法方法self.name
    class Meta:
        model = GoodsVisitCount
        fields = ['category','count']