from rest_framework import serializers
from goods.models import SPU,Brand
class SPUSerializer(serializers.ModelSerializer):
    """spu数据模型序列化器"""
    brand = serializers.StringRelatedField()  #string只作用于序列化，不能作为反序列化校验
    brand_id = serializers.IntegerField()
    category1_id = serializers.IntegerField()
    category2_id = serializers.IntegerField()
    category3_id = serializers.IntegerField()
    class Meta:
        model = SPU
        exclude = ['category1','category2','category3']

class BrandSerializer(serializers.ModelSerializer):
    """品牌展示序列化器"""
    class Meta:
        model = Brand
        fields = ['id','name']