from rest_framework import serializers
from goods.models import Brand



class BrandManagementSerializer(serializers.ModelSerializer):
    """品牌管理序列化器"""
    class Meta:
        model = Brand
        fields = '__all__'