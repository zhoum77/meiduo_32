from rest_framework import serializers
from goods.models import GoodsChannel,GoodsChannelGroup
class ChannelManagementSerializer(serializers.ModelSerializer):
    """频道管理序列化器"""
    group = serializers.StringRelatedField()  #只用于序列化
    group_id = serializers.IntegerField()
    category = serializers.StringRelatedField()
    category_id = serializers.IntegerField()


    class Meta:
        model = GoodsChannel
        fields = '__all__'

class ChannelGroupSerializer(serializers.ModelSerializer):
    """频道分组序列化器"""
    class Meta:
        model = GoodsChannelGroup
        fields = '__all__'