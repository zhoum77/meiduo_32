from rest_framework import serializers
from goods.models import SKU,SKUSpecification,GoodsCategory,SPU,SpecificationOption,SPUSpecification

class SKUSpecSerializer(serializers.ModelSerializer):
    """sku规格选项模型类序列化器"""
    spec_id = serializers.IntegerField()
    option_id = serializers.IntegerField()
    class Meta:
        model = SKUSpecification
        fields = [
            'spec_id',
            'option_id'
        ]

class SKUManagementSerializer(serializers.ModelSerializer):
    """sku商品管理模型类序列化器"""
    spu = serializers.StringRelatedField()
    spu_id = serializers.IntegerField()   #主表id，用整型
    category_id = serializers.IntegerField()
    category = serializers.StringRelatedField()
    specs = SKUSpecSerializer(many=True)                              #从表字段用模型集映射

    class Meta:
        model = SKU
        fields = '__all__'
        #根据模型类得到的字段只有显示的字段，非主键的隐藏字段不会自动映射
        #非主键隐藏字段：主表id，从表模型集
    def create(self, validated_data):
        """重写create方法，在新增sku表的同时，还需要新增sku规格选项信息表的数据"""
        #从已校验数据获取specs，并删除
        specs = validated_data.pop('specs')
        #创建sku表数据
        instance = SKU.objects.create(**validated_data)
        #创建sku规格选项信息表

        for temp in specs:
            # 添加sku_id
            temp['sku_id'] = instance.id
            SKUSpecification.objects.create(**temp)
        return instance

    #在进行sku商品修改时，涉及到两张表的更新，需要重写update方法
    def update(self, instance, validated_data):
        #1.获取sku规格选项表更新信息
        # 从已校验数据获取specs，并删除
        specs = validated_data.pop('specs')
        #2.更新sku表  再继承父类方法
        instance = super().update(instance,validated_data)
        #3.删除规格表原有数据
        SKUSpecification.objects.filter(sku_id=instance.id).delete()
        #4.增加数据
        for temp in specs:
            temp['sku_id'] = instance.id
            SKUSpecification.objects.create(**temp)
        return instance

class CategorySerializer(serializers.ModelSerializer):
    """分类商品模型序列化器"""
    class Meta:
        model = GoodsCategory
        fields = ['id','name']

class SPUSerializer(serializers.ModelSerializer):
    """SPU商品模型序列化器"""
    class Meta:
        model = SPU
        fields = ['id','name']

class SpecOptionSerializer(serializers.ModelSerializer):
    """规格选项信息序列化器"""
    spec = serializers.StringRelatedField()
    spec_id = serializers.IntegerField()
    class Meta:
        model = SpecificationOption
        fields = '__all__'
class SPUSpecSerializer(serializers.ModelSerializer):
    """SPU商品规格信息模型序列化器"""
    spu_id = serializers.IntegerField()
    spu = serializers.StringRelatedField()
    options = SpecOptionSerializer(many=True,read_only=True)

    class Meta:
        model = SPUSpecification
        fields = '__all__'