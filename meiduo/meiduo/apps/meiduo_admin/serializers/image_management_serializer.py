from rest_framework import serializers
from goods.models import SKUImage,SKU
from meiduo.utils.fastdfs.fastdfs_fileupload_handler import fdfs_fileupload_handler
class ImageManagementSerializer(serializers.ModelSerializer):
    """sku图片序列化器"""
    # sku = serializers.PrimaryKeyRelatedField(queryset=SKU.objects.all())
    class Meta:
        model = SKUImage
        fields = '__all__'



    #当图片被当作文件上传时，会自动调用存储后端来完成保存
    #如果被赋值成一个字符串，则不会调用存储后端，会直接将该字符存储在mysql表中
    # def create(self, validated_data):
    #     # 'image' = {InMemoryUploadedFile: 47247}01.jpeg  得到图片上传文件对象
    #     # 'sku' = {SKU}20: 13333    #得到sku对象
    #     #从校验后的数据可知，得到的image是一个文件对象，这个时候在进行新增数据时，会自动调用存储后端保存图片信息
    #     #但是，存储后端目前并没有实现保存的功能，我们可以对image手动上传，并进行转字符串处理
    #     #1.获取image图片文件对象
    #     image_file = validated_data.get('image')
    #     file_id = fdfs_fileupload_handler(image_file)
    #
    #     instance = SKUImage.objects.create(sku=validated_data.get('sku'),image=file_id)
    #     return instance
    #
    #
    # #同理，修改sku图片数据，也需要重写update方法
    # def update(self, instance, validated_data):
    #     image_file = validated_data.get('image')
    #     file_id = fdfs_fileupload_handler(image_file)
    #     validated_data['image'] = file_id
    #     instance = super().update(instance,validated_data)
    #     return instance

    #除了重写create和update方法外，我们还可以直接通过重写校验方法，从源头获取image字符串
    # def validate(self, attrs):
    #     image_file = attrs.get('image')
    #     file_id = fdfs_fileupload_handler(image_file)
    #     attrs['image'] = file_id
    #     return attrs

    #除了将image手动上传，并将file_id存到数据库中这种方法外，还可以通过修改存储后端方法来实现图片的上传