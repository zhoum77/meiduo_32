from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework_jwt.utils import jwt_encode_handler,jwt_payload_handler
class LoginSerializer(serializers.Serializer):
    """定义序列化器"""
    username = serializers.CharField(max_length=20,required=True,write_only=True)
    password = serializers.CharField(required=True,write_only=True)
    def validate(self, attrs):  #attrs字典类型，传入的是经过前续校验后的数据
        #1.获取数据
        username = attrs.get('username')
        password = attrs.get('password')
        #校验数据，用户是否存在
        user = authenticate(username=username,password=password)   #这个地方调用的是自定义用户认证后端
        # 不存在或者用户未激活就抛出异常
        if not user or not user.is_active:
            raise serializers.ValidationError('传统身份认证失败！')
        #存在即做状态保持，返回token给浏览器
        #签发token，返回有效数据
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return {'user':user,'token':token}