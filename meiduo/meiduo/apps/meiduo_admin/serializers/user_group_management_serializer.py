from rest_framework import serializers
from django.contrib.auth.models import Group

class UserGroupManagementSerializer(serializers.ModelSerializer):
    """用户组序列化器"""
    class Meta:
        model = Group
        fields = ['id','name','permissions']


