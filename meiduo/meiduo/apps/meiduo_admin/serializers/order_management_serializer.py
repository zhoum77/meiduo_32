from rest_framework import serializers
from orders.models import OrderInfo,OrderGoods
from goods.models import SKU

class OrderManagementSerializer(serializers.ModelSerializer):
    """订单管理序列化器"""
    class Meta:
        model = OrderInfo
        fields = ['order_id','create_time','status']
        extra_kwargs = {
            'create_time':{'format':'%Y-%m-%d','read_only':True},
            'status':{'required':True}
        }

class SKUSerializer(serializers.ModelSerializer):
    """sku商品序列化器"""
    class Meta:
        model = SKU
        fields = ['name','default_image']




class OrderGoodsSerializer(serializers.ModelSerializer):
    """订单商品序列化器"""
    sku = SKUSerializer()
    class Meta:
        model = OrderGoods
        fields = ['count','price','sku']


class OrderDetailSerializer(serializers.ModelSerializer):
    """订单详细信息序列化器"""
    user = serializers.StringRelatedField()
    skus = OrderGoodsSerializer(many=True)
    class Meta:
        model = OrderInfo
        exclude = ['address']
        extra_kwargs = {
            'create_time': {'format':'%Y-%m-%d：%H时%M分%S秒'},
        }