from rest_framework import serializers
from users.models import User
from django.contrib.auth.hashers import make_password
class AdminSerializer(serializers.ModelSerializer):
    """管理员管理序列化器"""
    # group = serializers.ManyRelatedField()
    class Meta:
        model = User
        fields = ['id','username','email','mobile','password','groups','user_permissions']
        extra_kwargs = {
            'password':{'write_only':True},  #密码只作用于反序列化操作
            # 'group':{'required':True},
            # 'user_permissions':{'required':True},
        }

    #在进行新建和更新时，密码需要加密，is_staff=True，序列器无法帮我们直接完成这两项操作，需要我们重写validate方法
    def validate(self, attrs):
        #目的：密码加密存储，设置is_staff=True
        password = attrs.get('password')
        password = make_password(password)
        attrs['password'] = password
        attrs['is_staff'] = True
        return attrs