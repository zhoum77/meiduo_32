from django.conf.urls import url
from rest_framework import routers
from meiduo_admin.views.login_views import LoginView
from meiduo_admin.views.user_count_views import *
from meiduo_admin.views.user_management_views import UserListCreateView
from rest_framework_jwt.views import obtain_jwt_token
from meiduo_admin.views.sku_management_views import *
from meiduo_admin.views.spu_management_views import *
from meiduo_admin.views.speci_management_views import *
from meiduo_admin.views.image_management_views import *
from meiduo_admin.views.channel_management_views import ChannelManagementViewSet,GoodsCategoriesView
from meiduo_admin.views.brand_management_views import BrandManagementViewSet
from meiduo_admin.views.order_management_views import OrderManagementViewSet
from meiduo_admin.views.permission_management_views import *
from meiduo_admin.views.user_group_management_views import *
from meiduo_admin.views.admin_views import *
urlpatterns = [
    #登录路由
    # url(r'^authorizations/$',LoginView.as_view()),
    url(r'^authorizations/$',obtain_jwt_token),
    # #用户总数统计
    # url(r'^statistical/total_count/$',UserCountView.as_view({'get':'total_count'})),
    # #日增用户统计
    # url(r'^statistical/day_increment/$',UserCountView.as_view({'get':'day_increment'})),
    # #日活用户统计
    # url(r'^statistical/day_active/$',UserCountView.as_view({'get':'day_active'})),
    # #日下单用户量统计
    # url(r'^statistical/day_orders/$',UserCountView.as_view({'get':'day_orders'})),
    # #月增用户统计
    # url(r'^statistical/month_increment/$',UserCountView.as_view({'get':'month_increment'})),
    #日分类商品访问量
    url(r'^statistical/goods_day_views/$',CategoryDateVisitView.as_view()),
    #用户管理路由
    url(r'^users/$',UserListCreateView.as_view()),
    #sku商品管理路由
    url(r'^skus/$',SKUManagementView.as_view({'get':'list','post':'create'})),
    #获取三级分类信息
    url(r'^skus/categories/$',CategoryShowView.as_view()),
    #获取spu表名称数据
    url(r'^goods/simple/$',SPUShowView.as_view()),
    #获取SPU商品规格信息
    url(r'^goods/(?P<pk>\d+)/specs/$',SPUSpecShowView.as_view()),
    #实现sku一条商品的查询、更新和删除
    url(r'^skus/(?P<pk>\d+)/$',SKUModifyDelView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    #spu商品管理路由
    # url(r'^goods/$',SPUManagementViewSet.as_view({'get':'list','post':'create'})),
    #获取品牌数据路由
    # url(r'^goods/brands/simple/$',BrandShowView.as_view({'get':'list'})),  单独品牌视图下，添加的路由
    # 将品牌视图添加到spu管理视图集中（需要在视图方法添加action装饰器），这种情况下需要为spu视图集添加新的路由，匹配品牌展示视图方法
    # url(r'^goods/brands/simple/$',SPUManagementViewSet.as_view({'get':'get_brand'})),
    #添加一级商品展示路由
    url(r'^goods/channel/categories/$',CategoryOneView.as_view()),
    #添加二级、三级商品展示路由
    url(r'^goods/channel/categories/(?P<pk>\d+)/$',CategoryOneView.as_view()),
    # url(r'^goods/specs/$',SpecManagementViewSet.as_view({'get':'list'})),
    #在新增图片视图中，添加sku展示视图
    url(r'^skus/simple/$',SKUShowView.as_view()),
    #商品频道管理
    url(r'^goods/channels/$',ChannelManagementViewSet.as_view({'get':'list','post':'create'})),

    url(r'^goods/channels/(?P<pk>\d+)/$',ChannelManagementViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    #频道组
    url(r'^goods/channel_types/$',ChannelManagementViewSet.as_view({'get':'get_channelgroup'})),
    #新增频道中获取一类商品分类信息
    url(r'^goods/categories/$',GoodsCategoriesView.as_view()),
    #品牌管理
    url(r'^goods/brands/$',BrandManagementViewSet.as_view({'get':'list','post':'create'})),
    url(r'^goods/brands/(?P<pk>\d+)/$',BrandManagementViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    #订单管理,展示所有订单
    url(r'^orders/$',OrderManagementViewSet.as_view({'get':'list'})),
    #获取某一订单的详细信息
    url(r'^orders/(?P<pk>\d+)/$',OrderManagementViewSet.as_view({'get':'retrieve'})),
    #订单status的部分更新
    url(r'^orders/(?P<pk>\d+)/status/$',OrderManagementViewSet.as_view({'patch':'partial_update'})),
    #权限管理,获取所有权限信息
    url(r'^permission/perms/$',PermissionViewSet.as_view({'get':'list','post':'create'})),
    #新增权限前，展示所有权限类型
    url(r'^permission/content_types/$',ContentTypeView.as_view()),
    #获取、更新、删除单一权限
    url(r'^permission/perms/(?P<pk>\d+)/$', PermissionViewSet.as_view({'get': 'retrieve', 'put': 'update','delete':'destroy'})),
    #用户组管理，获取和新增所有用户组信息
    url(r'^permission/groups/$',UserGroupManagementViewSet.as_view({'get':'list','post':'create'})),
    #新增用户组时，需要展示所有权限选项
    url(r'^permission/simple/$',UserGroupManagementViewSet.as_view({'get':'get_permission'})),
    #获取、更新、删除单一用户组
    url(r'^permission/groups/(?P<pk>\d+)/$',UserGroupManagementViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    #获取admin用户信息
    url(r'^permission/admins/$',AdminViewSet.as_view({'get':'list','post':'create'})),
    #新增管理员前，需要展示所有组信息
    url(r'^permission/groups/simple/$',GroupsView.as_view()),
    # 获取、更新、删除单一管理员信息
    url(r'^permission/admins/(?P<pk>\d+)/$',AdminViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),

]
#1.创建路由对象
route = routers.SimpleRouter()
#2.注册视图集，只有视图集才能使用Router
route.register(prefix='statistical',viewset=UserCountView,basename='UserCount')
#注册spu规格视图集
route.register(prefix=r'goods/specs',viewset=SpecManagementViewSet,basename='spec')
#3.添加路由到路由映射表
# urlpatterns += route.urls
##借用路由集解决品牌视图添加到spu管理视图集中，需要添加新的路由问题
#注册路由集
route.register(prefix='goods',viewset=SPUManagementViewSet,basename='goods')
# urlpatterns += route.urls   # 添加路由到路由映射表
#route.urls
#[<RegexURLPattern UserCount-day-active ^statistical/day_active/$>,
# <RegexURLPattern UserCount-day-increment ^statistical/day_increment/$>,
# <RegexURLPattern UserCount-day-orders ^statistical/day_orders/$>,
# <RegexURLPattern UserCount-month-increment ^statistical/month_increment/$>,
# <RegexURLPattern UserCount-total-count ^statistical/total_count/$>,
# <RegexURLPattern goods-list ^goods/$>,
# <RegexURLPattern goods-brandshow ^goods/brands/simple/$>,
# <RegexURLPattern goods-detail ^goods/(?P<pk>[^/.]+)/$>]
#注册spu选项视图集
route.register(prefix='specs/options',viewset=OptionManagementViewSet,basename='option')
#注册sku图片视图集
route.register(prefix='skus/images',viewset=ImageManagementViewSet,basename='images')
urlpatterns += route.urls   # 添加路由到路由映射表

'rest_framework_jwt.utils.jwt_response_payload_handler'
from rest_framework_jwt.utils import jwt_response_payload_handler