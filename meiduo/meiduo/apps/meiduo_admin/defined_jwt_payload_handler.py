
#自定义jwt拓展构建响应数据
#原本构建响应对象的方法jwt_response_payload_handler只返回token，不符合业务需求，
# 而我们又发现jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
#用defined_jwt_payload_handler方法覆盖jwt_response_payload_handler方法
#并在设置中指定WT_RESPONSE_PAYLOAD_HANDLER即可
def defined_jwt_payload_handler(token, user=None, request=None):
    """
    Returns the response data for both the login and refresh views.
    Override to return a custom response such as including the
    serialized representation of the User.

    Example:

    def jwt_response_payload_handler(token, user=None, request=None):
        return {
            'token': token,
            'user': UserSerializer(user, context={'request': request}).data
        }

    """
    return {
        'username':user.username,
        'user_id':user.id,
        'token': token
    }