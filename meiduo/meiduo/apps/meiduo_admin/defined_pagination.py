from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
class MyPage(PageNumberPagination):
    """自定义分页器"""
    page_size_query_param = 'pagesize'  #查询字符串中，指明每页页数的key: ?page=2&pagesize=3
    page_size = 1
    #重写get_paginated_response方法，修改返回的数据
    def get_paginated_response(self, data):
        return Response({
            "counts": self.page.paginator.count,  #总数量
            "lists": data,   #用户数据
            "page": self.page.number,   #当前页码
            "pages": self.page.paginator.num_pages,   #总页码
            "pagesize": self.page_size,  #页容量
        })
