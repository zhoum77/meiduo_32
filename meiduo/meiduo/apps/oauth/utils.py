from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,BadData
from django.conf import settings

def generate_open_id_signature(openid):
    """对原始的openid加密"""
    #1.创建Serializer实例化对象（密钥，过期时间：秒）
    serializer = Serializer(secret_key=settings.SECRET_KEY,expires_in=600)
    #2.调用加密实例化对象中的dumps({}) 返回byte类型
    openid_sign = serializer.dumps({'openid':openid})
    #3.返回str类型  加密后的openid
    return openid_sign.decode()


def check_open_id(openid_sign):
    """对加密后的openid进行解密"""
    # 1.创建Serializer实例化对象（密钥，过期时间：秒）密钥和过期时间应该和当初加密时设置成一样
    serializer = Serializer(secret_key=settings.SECRET_KEY, expires_in=600)
    # 2.调用加密实例化对象中的loads方法，字符串转字典类型
    try:
        openid_sign = serializer.loads(openid_sign)
        # 3.返回字典类型  解密后的openid
        openid = openid_sign.get("openid")
        return openid
    except BadData:   #解不出来的情况
        return None