from django.shortcuts import render,redirect
from django.views import View
from django.http import JsonResponse
from django.contrib.auth import login
from django.conf.global_settings import SESSION_COOKIE_AGE
from django.http import *
from django_redis import get_redis_connection

from meiduo.utils.response_code import RETCODE
from .models import OAuthQQUser
from users.models import User
from QQLoginTool.QQtool import OAuthQQ
from .utils import generate_open_id_signature,check_open_id
from carts.utils import merge_cart_cookie_to_redis
import re
# Create your views here.
class QQAuthURLView(View):
    """拼接QQ登录url"""
    def get(self,request): #this.host + '/qq/authorization/?next=' + next
        #接收查询参数
        next = request.GET.get('next')
        #创建QQTOOL中的工具对象，调用获取路径方法
        qq_tool = OAuthQQ(client_id='101568493', client_secret='e85ad1fa847b5b79d07e40f8f876b211', redirect_uri='http://www.meiduo.site:8000/oauth_callback', state=next)
        login_url = qq_tool.get_qq_url()
        return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK','login_url':login_url})

class QQAuthUserView(View):
    """用户扫码登录的回调处理"""
    def get(self,request):
        # 1.获取code数据
        query_dict = request.GET
        code = query_dict.get('code')

        # 2.创建qq工具对象调用get_access_token方法，得到token
        qq_tool = OAuthQQ(client_id='101568493', client_secret='e85ad1fa847b5b79d07e40f8f876b211',redirect_uri='http://www.meiduo.site:8000/oauth_callback', state=next)
        qq_token = qq_tool.get_access_token(code)
        # 3.调用get_open_id方法，得到openid
        openid = qq_tool.get_open_id(qq_token)
        # 4.将该openid与QQ登录用户数据库中的openid进行匹配
        # 4.1如果能匹配成功说明已经存在用户，只需要与之关联
        try:
            qq_user = OAuthQQUser.objects.get(openid=openid)
            # 如果openid查询到了(openid已绑定用户, 代表登录成功)
            master_user = qq_user.user
            #状态保持
            login(request,master_user)
            #将username作为cookie传给前端接收
            response = redirect(query_dict.get('state') or '/')
            response.set_cookie("username",master_user.username,max_age=SESSION_COOKIE_AGE)
            #合并购物车
            merge_cart_cookie_to_redis(request, response)
            #重定向至来源
            return response
        # 4.2如果不能匹配成功，说明openid还没有绑定用户，那就到渲染绑定页面绑定手机号
        except:
            # 对原始的openid 进行加密,把加密后的openid再渲染到表单
            openid_sign = generate_open_id_signature(openid)
            context = {"openid":openid_sign}
            return render(request,'oauth_callback.html',context)  #将openid渲染到jinja2模板中

#返回的网页http://www.meiduo.site:8000/oauth_callback?code=82A449EDE301421BF6BFE8D039EA33D0&state=%2F
    def post(self,request):
        """绑定用户页面"""
#渲染后的网址是一样的 http://www.meiduo.site:8000/oauth_callback/?code=C4392D991F20EF2EA3528B74552BA9D2&state=%2F
        #1.接收post表单数据
        query_dict = request.POST
        mobile = query_dict.get("mobile")
        password = query_dict.get("password")
        sms_code = query_dict.get("sms_code")
        openid_sign = query_dict.get("openid")   #openid通过表单一起传递给了后端
        #2.校验
        # 一般从前端传来的数据都应该是有的，这个判断是防止非法访问
        if all([mobile,password,sms_code,openid_sign]) is False:
            return HttpResponseForbidden("缺少必传参数")
        if re.match(r'^1[3-9]\d{9}$',mobile) is None:
            return HttpResponseForbidden('手机号格式有误')
        if not re.match(r'^[0-9A-Za-z]{8,20}$', password):
            return HttpResponseForbidden('请输入8-20个字符的密码')
        #判断短信验证码是否过期
        # 短信验证码会存到redis中，设置了过期时间为5分钟,可以通过取值判断是否过期
        conn = get_redis_connection(alias="verify_code")
        sms_code_byte = conn.get('sms_code%s' %mobile)  #这里得到的是byte类型
        if sms_code_byte is None:
            return HttpResponseForbidden('短信验证码已过期')
        #判断短信验证码是否匹配
        if sms_code_byte.decode("utf-8") != sms_code:
            return HttpResponseForbidden('短信验证码不正确')
        #3.业务逻辑处理
        #判断该手机号是否已经存在在用户数据库中
        try:
            master_user = User.objects.get(mobile=mobile)
            #如果用户已经存在，还要判断密码是否匹配
            if master_user.check_password(password) is False:
                return HttpResponseForbidden("绑定失败")
        except User.DoesNotExist:
            #如果用户数据库中还没有该用户，则添加用户
            master_user = User.objects.create_user(username=mobile,mobile=mobile,password=password)
        # 对openid进行解密
        openid = check_open_id(openid_sign)
        #如果没有取到openid
        if openid is None:
            return HttpResponseForbidden('openid无效')
        #取到openid，创建qq用户对象并与主用户数据库对应的用户绑定
        OAuthQQUser.objects.create(
            user = master_user,
            openid = openid
        )
        #状态保持
        login(request,master_user)
        #记住username
        response = redirect(request.GET.get("state") or '/')
        response.set_cookie("username",master_user.username,max_age=SESSION_COOKIE_AGE)
        #4.响应，重定向至来源
        #合并购物车
        merge_cart_cookie_to_redis(request, response)
        return response
