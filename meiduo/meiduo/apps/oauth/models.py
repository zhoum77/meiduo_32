from django.db import models
from meiduo.utils.models import BaseModel
from users.models import User
# Create your models here.
#创建qq登录模型类
class OAuthQQUser(BaseModel):
    user = models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='用户')
    openid = models.CharField(max_length=64,verbose_name='openid',db_index=True)  #设置索引

    class Meta:
        db_table = 'tb_oauth_qq'
        verbose_name = 'QQ登录用户数据'
        verbose_name_plural = verbose_name