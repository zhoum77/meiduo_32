from django.shortcuts import render
from django.views import View
from django.conf import settings
from django.http import JsonResponse,HttpResponseForbidden
from django_redis import get_redis_connection
from django.contrib.auth import login
from meiduo.utils.sinaweibopy3 import APIClient
from meiduo.utils.response_code import RETCODE
from .models import OAuthSinaUser
from users.models import User
from oauth.utils import generate_open_id_signature,check_open_id
from carts.utils import merge_cart_cookie_to_redis
import json,re
# Create your views here.
class WeiboURLView(View):
    """拼接微博认证url"""
    def get(self,request):
        #创建APIClient对象
        apiclient = APIClient(
            app_key=settings.APP_KEY,
            app_secret=settings.APP_SECRET,
            redirect_uri=settings.REDIRECT_URL
        )
        #生成跳转的授权地址
        login_url =apiclient.get_authorize_url()
        return JsonResponse({'code':RETCODE.OK,'errmsg':'ok','login_url':login_url})

#sina_callback?code=5055892ed7ef78784be51f94be9d5962
class WeiboAuthUserView(View):
    """微博登录返回的接口"""
    def get(self,request):
        #直接将认证页面渲染出来
        return render(request,'sina_callback.html')


class WeiboBindUserView(View):
    """微博认证"""
    def get(self,request):
        # 1.接收数据
        code = request.GET.get('code')
        # 2.校验数据
        # 判断code是否为空
        if code is None:
            return HttpResponseForbidden('非法用户')
        # 3.1得到accesstoken，进行验证
        # 创建APIClient对象
        apiclient = APIClient(
            app_key=settings.APP_KEY,
            app_secret=settings.APP_SECRET,
            redirect_uri=settings.REDIRECT_URL
        )
        # 根据code值获取access_token和uid值
        result = apiclient.request_access_token(code)
        access_token = result.access_token
        uid = result.uid
        #这个地方注意：要将uid加密传出去
        uid_sign = generate_open_id_signature(uid)
        # 3.2判断用户是否绑定
        try:
            #查询用户信息，如果能找到对应的用户，说明已经绑定成功，直接登录
            sina_user = OAuthSinaUser.objects.get(uid=uid)
            user = sina_user.user
            #状态保持
            login(request,user)
            #记住username
            response = JsonResponse({'code': RETCODE.OK, 'errmsg': 'ok', 'token': uid_sign,'user_id':user.id,'username':user.username})
            response.set_cookie('username',user.username)
            #合并购物车
            merge_cart_cookie_to_redis(request,response)
            return response
        except OAuthSinaUser.DoesNotExist:
            #未查询到用户信息，需要先绑定
            return JsonResponse({'code':RETCODE.OK,'errmsg':'ok','access_token':uid_sign})

    # 3.3未绑定用户，渲染绑定页面


    def post(self,request):
        """未绑定用户，绑定页面"""
        #1.接收数据
        json_dict = json.loads(request.body.decode())
        password=json_dict.get('password')
        mobile=json_dict.get('mobile')
        sms_code=json_dict.get('sms_code')
        uid_sign=json_dict.get('access_token')
        #2.校验数据
        if not all(json_dict.values()):
            return HttpResponseForbidden('缺少必传参数')
        if not re.match(r'^[a-zA-Z0-9]{8,20}$',password):
            return HttpResponseForbidden('密码格式错误')
        if re.match(r'^1[3-9]\d{9}$',mobile) is None:
            return HttpResponseForbidden('手机号码有误')
        redis_conn = get_redis_connection('verify_code')
        sms_code_byte = redis_conn.get('sms_code%s' % mobile)
        if sms_code_byte.decode() != sms_code:
            return HttpResponseForbidden('短信验证码错误')
        #3.绑定用户表和微博用户表（已存在用户，未绑定微博；不存在用户，直接绑定微博）
        try:
            #先判断该手机号对应的用户是否已存在
            user = User.objects.get(mobile=mobile)
            #还要校验密码，如果密码也正确，那直接将该用户和微博绑定
            if user.check_password(password) is False:
                return HttpResponseForbidden('绑定失败')
        except User.DoesNotExist:
        #如果用户不存在，则新建一个用户
            user = User.objects.create_user(
                username=mobile,
                mobile=mobile,
                password=password,
            )
        #对uid进行解密
        uid = check_open_id(uid_sign)
        if uid is None:
            return HttpResponseForbidden('uid失效')
        #绑定微博数据库表
        OAuthSinaUser.objects.create(
            user=user,
            uid=uid,
        )
        #4.状态保存
        login(request,user)
        #记住username
        response = JsonResponse({'code':RETCODE.OK,'errmsg':'ok','token':uid,'user_id':user.id,'username':user.username})
        response.set_cookie("username",user.username)
        # 合并购物车
        merge_cart_cookie_to_redis(request, response)
        #6.响应json
        return response