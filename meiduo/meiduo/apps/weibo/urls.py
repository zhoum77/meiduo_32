from django.conf.urls import url
from .views import WeiboURLView,WeiboAuthUserView,WeiboBindUserView

urlpatterns = [
    #拼接微博认证url
    url(r'^weibo/authorization/$',WeiboURLView.as_view()),
    #微博登录返回的认证
    url(r'^sina_callback$',WeiboAuthUserView.as_view()),
    #微博绑定
    url(r'^oauth/sina/user/$',WeiboBindUserView.as_view()),
]