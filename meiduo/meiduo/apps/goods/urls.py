from django.conf.urls import url
from .views import ListView,HotGoodsView,GoodsDetailView,GoodsVisitView,GoodsCommentsView

urlpatterns = [
    #商品列表页面展示"
    url(r'^list/(?P<category_id>\d+)/(?P<page_num>\d+)/$',ListView.as_view()),
    #热销商品展示
    url(r'^hot/(?P<category_id>\d+)/$',HotGoodsView.as_view()),
    #商品详情页面展示
    url(r'^detail/(?P<sku_id>\d+)/$',GoodsDetailView.as_view()),
    # 统计分类商品访问量
    url(r'^visit/(?P<category_id>\d+)/$',GoodsVisitView.as_view()),
    # 分类商品评论展示        /comments/'+ this.sku_id +'/
    url(r'^comments/(?P<sku_id>\d+)/$',GoodsCommentsView.as_view()),
    ]