from django.shortcuts import render
from django.views import View
from django.http import HttpResponseForbidden,JsonResponse
from django.core.paginator import Paginator
from django.utils import timezone
from .models import GoodsCategory,SKU,GoodsVisitCount
from .utils import breadcrumb
from contents.utils import get_category
from meiduo.utils.response_code import RETCODE
from meiduo.utils.views import LoginRequireView
from orders.models import OrderInfo
# Create your views here.

class ListView(View):
    """商品列表页面展示"""
    def get(self,request,category_id,page_num):
        #1.获取数据
        #category_id 代表商品类别表中的三级标题id， page_num代表查询的页数
        #sort=price  获取商品的展示方式，如果没有，默认为default
        sort = request.GET.get('sort','default')
        #2.校验
        #防止恶意访问
        try:
            cat3 = GoodsCategory.objects.get(id=category_id)  #获取对应的三级标题对象
        except GoodsCategory.DoesNotExist:
            return HttpResponseForbidden("id错误")
        #3.业务逻辑处理
        #3.0按哪种方式排序
        if sort == 'price':
            sort_page = '-price'
        elif sort == 'hot':
            sort_page = '-sales'
        else:
            sort = 'default'
            sort_page = 'create_time'
        #3.1获取商品三级分类展示
        categories = get_category()
        #3.2面包屑导航


        page = 5

        #3.3 获取三级分类下所有的SKU商品
        sku_qs = cat3.sku_set.filter(is_launched=True).order_by(sort_page)
        # 3.4  获取总页数
        # total_page = sku_qs.count() // page + 1 if sku_qs.count() % page else 0
        # 3.5 获取指定页的商品
        page_num = int(page_num)
        # sku_index = sku_qs[(page_num-1)*page:(page_num-1)*page+page]
        #3.6 django已经帮我们封装了分页的方法
        paginator = Paginator(sku_qs,page)
        try:
            sku_index = paginator.page(page_num)  #获取指定页的商品
            total_page = paginator.num_pages   #获取总页数
        except:
            return HttpResponseForbidden("查不到指定页")

        #4.响应
        context = {
            'categories':categories,
            'breadcrumb':breadcrumb(cat3),
            'category': cat3,
            'sort':sort,
            'page_skus':sku_index,  # 指定页的sku数据
            'page_num':page_num,   #当前显示的是第几页
            'total_page':total_page,  #总页数
        }
        return render(request,'list.html',context)

class HotGoodsView(View):
    """热销商品展示"""
    def get(self,request,category_id):
        #1.接收数据
        #2.校验
        #检验该三级标题是否存在
        try:
            cat3 = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return HttpResponseForbidden('id不存在')
        #3.业务逻辑处理
        #3.1查询该三级标题下所有的商品，按照销量排序
        sku_qs = cat3.sku_set.order_by('-sales')
        #3.2截取前两个作为展示
        sku_qs_limit = sku_qs[0:2]
        #4.响应json数据，hot_skus
        #4.1包装成json数据
        hot_skus_list = []
        for sku in sku_qs_limit:
            hot_skus_dict = {
                'id':sku.id,
                'name':sku.name,
                'default_image_url':sku.default_image.url,
                'price':sku.price,
            }
            hot_skus_list.append(hot_skus_dict)
        return JsonResponse({'code':RETCODE.OK,'errmgs':'ok','hot_skus':hot_skus_list})

class GoodsDetailView(View):
    """商品详情页展示"""
    def get(self,request,sku_id):
        #1.获取数据
        #2.校验
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('商品不存在')
        #3.业务逻辑处理
        cat3 = sku.category  #获取对应的三级标题
        spu = sku.spu  #获取对应的SPU
        """1.准备当前商品的规格选项列表 [8, 11]"""
        # 获取出当前正显示的sku商品的规格选项id列表
        current_sku_spec_qs = sku.specs.order_by('spec_id')
        current_sku_option_ids = []  # [8, 11]
        for current_sku_spec in current_sku_spec_qs:
            current_sku_option_ids.append(current_sku_spec.option_id)

        """2.构造规格选择仓库
        {(8, 11): 3, (8, 12): 4, (9, 11): 5, (9, 12): 6, (10, 11): 7, (10, 12): 8}
        """
        # 构造规格选择仓库
        temp_sku_qs = spu.sku_set.all()  # 获取当前spu下的所有sku
        # 选项仓库大字典
        spec_sku_map = {}  # {(8, 11): 3, (8, 12): 4, (9, 11): 5, (9, 12): 6, (10, 11): 7, (10, 12): 8}
        for temp_sku in temp_sku_qs:
            # 查询每一个sku的规格数据
            temp_spec_qs = temp_sku.specs.order_by('spec_id')
            temp_sku_option_ids = []  # 用来包装每个sku的选项值
            for temp_spec in temp_spec_qs:
                temp_sku_option_ids.append(temp_spec.option_id)
            spec_sku_map[tuple(temp_sku_option_ids)] = temp_sku.id

        """3.组合 并找到sku_id 绑定"""
        spu_spec_qs = spu.specs.order_by('id')  # 获取当前spu中的所有规格

        for index, spec in enumerate(spu_spec_qs):  # 遍历当前所有的规格
            spec_option_qs = spec.options.all()  # 获取当前规格中的所有选项
            temp_option_ids = current_sku_option_ids[:]  # 复制一个新的当前显示商品的规格选项列表
            for option in spec_option_qs:  # 遍历当前规格下的所有选项
                temp_option_ids[index] = option.id  # [8, 12]
                option.sku_id = spec_sku_map.get(tuple(temp_option_ids))  # 给每个选项对象绑定下他sku_id属性

            spec.spec_options = spec_option_qs  # 把规格下的所有选项绑定到规格对象的spec_options属性上

        #4.渲染页面
        context = {
            'categories':get_category(),
            'breadcrumb':breadcrumb(cat3),
            'sku':sku,
            'category':cat3,
            'spu':spu,
            'spec_qs':spu_spec_qs,  # 当前商品的所有规格数据
        }
        return render(request,'detail.html',context)

class GoodsVisitView(View):
    """商品三级类别统计访问量"""
    def post(self,request,category_id):
        #1.接收数据  正则匹配出的数据返回都是字符串
        #2.校验数据
        #查看该三级id是否存在
        try:
            cat3 = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return HttpResponseForbidden('id不存在')
        #3.业务逻辑处理
        #需要单独建立商品访问量的数据库表，查看数据,如果能查到说明今天已有访问，未查到说明没有访问过
        today = timezone.now()
        try:
            # 3.1如果该商品类当天已经被访问过，则修改该数据的count+1
            goods_visit = cat3.goodsvisitcount_set.get(date=today)

        except:
            # 3.1如果该商品类当天还未被访问，则新增一条数据

            goods_visit = GoodsVisitCount()
            goods_visit.category_id = category_id
        goods_visit.count += 1
        goods_visit.save()
        #4.响应json
        return JsonResponse({'code':RETCODE.OK,'errmsg':'OK'})

class GoodsCommentsView(View):
    """商品类评论展示"""
    def get(self,request,sku_id):
        #1.获取数据
        #2.校验数据
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('查无此商品')
        #3.业务逻辑处理
        comment_list = []
        order_goods_qs = sku.ordergoods_set.filter(is_commented=True)
        for order_goods in order_goods_qs:
            order = order_goods.order
            origin_username = order.user.username
            username = origin_username[:1]+'****'+origin_username[-3:-1]
            comment_list.append({
                'username':username,
                'content':order_goods.comment,
            })
        #4.响应json
        return JsonResponse({'code':RETCODE.OK,'errmsg':'OK','comment_list':comment_list})

