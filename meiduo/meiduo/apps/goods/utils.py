
def breadcrumb(cat3):
    """面包屑导航"""
    cat2 = cat3.parent  # 获取对应的二级标题对象
    cat1 = cat2.parent  # 获取对应的一级标题对象
    cat1.url = cat1.goodschannel_set.all()[0].url  # cat1.goodschannel_set.all()获得的只是qs，取下标才是对象
    breadcrumb = {
        'cat1': cat1,
        'cat2': cat2,
        'cat3': cat3,
    }
    return breadcrumb