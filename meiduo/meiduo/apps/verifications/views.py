from django.views import View
from django.http import HttpResponse,JsonResponse,HttpResponseForbidden
from django_redis import get_redis_connection

from meiduo.libs.captcha.captcha import captcha
from meiduo.utils.response_code import RETCODE
from . import constants
from celery_tasks.sms.tasks import send_sms_code
import random
import logging
# 创建日志输出器对象
logger = logging.getLogger('django')  #因为在工程配置文件中定义了一个名为django的日志器
# Create your views here.
# 图形验证码
#  <img :src="image_code_url" alt="图形验证码" class="pic_code" @click="generate_image_code">
#  :scr 也会向后端发起请求，只能是get请求
class ImageCodeView(View):
    def get(self,request,uuid):
        #1.调用sdk 生成图形验证码
        # name: 唯一标识, text: 图形验证码字符串, image: 图形验证码图形bytes数据
        name, text, image = captcha.generate_captcha()
        # 2.创建redis连接对象
        conn = get_redis_connection(alias='verify_code')
        #3.获取图形验证码存入到redis，将来作匹配
        conn.setex(uuid,constants.IMAGE_CODE_EXPIRE,text)
        # 4. 响应图片数据
        return HttpResponse(image,content_type='image/png')
#短信验证码
#向后端接口发送请求，让后端发送短信验证码
#'/sms_codes/' + this.mobile + '/?image_code=' + this.image_code + '&uuid=' + this.uuid;
class SMSCodeView(View):
    def get(self,request,mobile):
        #0.为防止60s内频繁获取验证码，先判断标记是否还在
        # 创建redis连接对象
        conn = get_redis_connection(alias='verify_code')
        sms_flag = conn.get('sms_flag%s' %mobile)
        if sms_flag is not None:
            return JsonResponse({'code': RETCODE.THROTTLINGERR, 'errmsg': '频繁发送短信'})
        # 1.获取查询参数中的数据
        image_code = request.GET.get('image_code')
        uuid = request.GET.get('uuid')
        #2.校验
        #2.1 检验参数是否存在
        if all([image_code,uuid]) is False:
            return HttpResponseForbidden('缺少必传参数')
        #2.2和redis中的图形验证码进行比较
        #2.2.1创建redis连接对象

        #2.2.2取出redis中的uuid对应的text，如果时间太长uuid过期
        redis_uuid = conn.get(uuid)
        if redis_uuid is None:
            return JsonResponse({'code': RETCODE.IMAGECODEERR, 'errmsg': '图形验证码已过期'})
        # 2.2.3和前端传来的uuid作匹配(这个地方要注意：从redis取到的数据都是byte类型，需要转换)
        #最好做一个大小写都可，人性化匹配。将两边的字符串都转换成大写或都转成小写进行比较
        redis_uuid_d = redis_uuid.decode()
        if redis_uuid_d.lower() != image_code.lower():
            return JsonResponse({'code':RETCODE.THROTTLINGERR,'errmsg':'图形验证码填写错误'})
        #3.发送短信验证码
        #CCP().send_template_sms('17600992168', ['1234', 5], 1)
        #3.1发送随即生成的短信
        sms_num = '%06d' %random.randint(0,constants.MAX_SMS_NUM)
        #下面这行代码是阻塞型输出，会一直等待，直到平台发出短信才会执行代码以下的程序，影响服务器性能
        #发送短信是耗时的操作。如果短信被阻塞住，用户响应将会延迟。
        # 响应延迟会造成用户界面的倒计时延迟。
        #需要将这个任务单独创一个进程来执行，使得该代码是否执行并不影响下面代码的进行
        # CCP().send_template_sms('18507497208',[sms_num,6],1)
        send_sms_code.delay(mobile,sms_num)
        #通过log日志输出
        logger.info(sms_num)
        # log(sms_num)
        # 4.将生成的短信验证码存在redis中，准备将来注册时验证
        # 增加redis的管道功能，优化redis性能
        #创建redis管道对象
        pl = conn.pipeline()
        #用管道打包操作，这时操作不会执行
        pl.setex('sms_code%s' %mobile,constants.SMS_CODE_EXPIRE,sms_num)
        #4.1 为防止60s不到有人刷新页面重新获取短信验证码，可以在第一次发送验证码时生成一个标记一同存放到redis中,过期时间为60s
        #下一次同一号码请求时，会先从redis获取这个标记，如果有则说明没有到时间不能频繁获取，如果没有，说明该号码未获取过验证码或者时间已经超过60s
        pl.setex('sms_flag%s' %mobile,constants.SMS_FLAG_EXPIRE,1)
        #执行管道，这时才会将前面打包的操作一并执行，一次操作redis执行多个操作，提升redis性能
        pl.execute()
        #匹配成功响应Json数据
        return JsonResponse({'code':RETCODE.OK,'errmsg': '发送短信成功'})   #responseType: 'json',需要向前端响应Json数据