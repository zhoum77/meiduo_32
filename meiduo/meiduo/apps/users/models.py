from django.db import models
from django.contrib.auth.models import AbstractUser

from meiduo.utils.models import BaseModel
from areas.models import Area
# Create your models here.
#django自带用户认证系统，已经给我们提供了用户模型类，当中有些类属性（字段）我们是可以直接继承用的，
# 没有的只需要在自己定义的用户模型中定义属性就可以了
#修改了原有的User类需要重新配置，否则程序不会执行到自定义的这个用户模型类。
# Django用户模型类是通过全局配置项 AUTH_USER_MODEL 决定的
class User(AbstractUser):
    """自定义用户模型类"""
    mobile = models.CharField(max_length=11,unique=True,verbose_name='手机号')
    email_active = models.BooleanField(default=False,verbose_name='邮箱激活状态') #添加邮箱机会状态字段，用来判断邮箱
    default_address = models.ForeignKey('Address',on_delete=models.SET_NULL,related_name='users',null=True,blank=True,verbose_name='默认地址')
    #方便后台站点管理
    class Meta:
        db_table = "users_user"
        verbose_name = "用户"
        verbose_name_plural = verbose_name


class Address(BaseModel):  #继承这个基类，有创建时间字段、更新时间字段
    """用户收货地址"""
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='addresses',verbose_name='用户')
    title = models.CharField(max_length=20,verbose_name='地址名称')
    reciver = models.CharField(max_length=20,verbose_name='收货人')
    province = models.ForeignKey(Area,on_delete=models.PROTECT,related_name='province_addresses',verbose_name='省')
    city = models.ForeignKey(Area,on_delete=models.PROTECT,related_name='city_addresses',verbose_name='市')
    district = models.ForeignKey(Area,on_delete=models.PROTECT,related_name='district_address',verbose_name='区')
    place = models.CharField(max_length=50,verbose_name='详细地址')
    mobile = models.CharField(max_length=11,verbose_name='手机')
    tel = models.CharField(max_length=20,null=True,verbose_name='固定电话')
    email = models.CharField(max_length=30,null=True,blank=True,default='',verbose_name='电子邮箱')
    is_deleted = models.BooleanField(default=False,verbose_name='逻辑删除')

    class Meta:
        db_table = 'tb_addresses'
        verbose_name = '用户地址'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']

