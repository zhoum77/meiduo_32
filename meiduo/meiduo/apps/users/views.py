from django.shortcuts import render,redirect
from django.views import View
from django.http import HttpResponse,JsonResponse,HttpResponseForbidden
from django.contrib.auth import login,authenticate,logout
from django_redis import get_redis_connection
from django.db.models import Q
from django.conf.global_settings import SESSION_COOKIE_AGE
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.core.mail import send_mail


from .models import User,Address
from meiduo.utils.views import LoginRequireView
from meiduo.utils.response_code import RETCODE
from celery_tasks.emails.tasks import send_emails
from .utils import generate_email_verify_url,get_user_token
from goods.models import SKU
from carts.utils import merge_cart_cookie_to_redis
import re
import json
# Create your views here.
class RegisterViews(View):
    def get(self,request):
        return render(request,'register.html')
    def post(self,request):
        #1.接收浏览器发来的请求
        username = request.POST.get('username')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        mobile = request.POST.get('mobile')
        sms_code = request.POST.get('sms_code')
        allow = request.POST.get('allow')
        #2.对请求数据进行验证（一般来说，从前端过来的数据不需要进行验证，但为了防止非前端的恶意请求，需要进行数据验证）
        #all[列表]返回一个布尔值,判断元祖内的参数是否为空,这些数据是否都有
        if all([username,password,password2,mobile,sms_code]) is False:
            return HttpResponse("缺少相应的参数")
        #用正则判断用户名的规范性,如果匹配不成功，会返回None
        if not re.match(r'^[a-zA-Z0-9_-]{5,20}$',username):
            return HttpResponse("请输入5-20个字符的用户")
        # 用正则判断密码的规范性,如果匹配不成功，会返回None
        if not re.match(r'^[0-9A-Za-z]{8,20}$',password):
            return HttpResponse('请输入8-20个字符的密码')
        #判断两次输入的密码是否一致
        if password2 != password:
            return HttpResponse("两次输入的密码不一致")
        #用正则判断手机号的规范性,如果匹配不成功，会返回None
        if not re.match(r'^1[345789]\d{9}$',mobile):
            return HttpResponse("您输入的手机号格式不正确")
        #2.1判断短信验证码是否正确
        #2.1.1首先判断验证码是否失效（存在redis的验证码5分钟失效）
        # 连接redis数据库查看验证码是否失效
        conn = get_redis_connection('verify_code')
        sms_code_bytes = conn.get('sms_code%s' %mobile)
        if sms_code_bytes is None:
            context = {
                "register_errmsg":"验证码失效",
            }
            return render(request, "register.html", context)
        #2.1.2 如果短信验证码未失效，则进行匹配，匹配成功就继续操作
        if sms_code != sms_code_bytes.decode("utf-8"):
            context = {
                "register_errmsg": "验证码不正确",
            }
            return render(request, "register.html", context)
        #3.对数据库中的数据进行增删改查(这里需要把注册信息增加到数据库中)
        #增加数据时注意：密码不能原值保存到数据库，很危险，需要加密后保存
        #django已经封装了加密保存数据的方法create_user()，可以直接调用,返回一个模型对象
        user = User.objects.create_user(username=username,password=password,mobile=mobile)
        #3.1 状态保持 对于注册成功的用户，会直接跳转页面到首页，并记住用户的登陆状态
        # 注册成功后即表示用户登入成功
        login(request, user)
        response = redirect('/')
        response.set_cookie('username',user.username)
        #4.返回响应数据
        return response

#用户名重复注册
#通过查看前端js代码发现，前端通过发起axios请求，让后端检查是否有重名
#除了form表单可以发送post请求，其他的发送的一般是get请求
#url = this.host + '/usernames/' + this.username + '/count/'
#功能实现逻辑：查找数据库中该用户名的个数，如果个数为1说明已经存在（该字段已经设置为唯一），
# 如果个数为0，说明不存在，将count以json格式传给前端
class CheckUsername(View):
    def get(self,request,username):
        count = User.objects.filter(username=username).count()
        return JsonResponse({'count':count})          #根据请求,响应json数据
#手机号重复注册（同用户名重复注册）
#this.host + '/mobiles/' + this.mobile + '/count/'
class CheckMobile(View):
    def get(self,request,mobile):
        count = User.objects.filter(mobile=mobile).count()
        return JsonResponse({'count':count})          #根据请求,响应json数据

class LoginViews(View):
    """登录页面"""
    def get(self,request):
        return render(request,"login.html")

    def post(self,request):
        # 1.接收数据
        query_dict = request.POST
        username = query_dict.get('username')
        password = query_dict.get('password')
        remembered = query_dict.get('remembered')
        #还要接收查询参数部分，请求的来源 http://127.0.0.1:8000/login/?next=/info/
        # next = request.GET.get("next")
        # # 判断next
        # if next is None:
        #     next = '/'

        #2.校验数据
        #这个地方需要检查数据来源，如果不是前端发来的数据有可能出现空值
        if all([username,password]) is False:
            return HttpResponseForbidden("缺少传递参数")
        #3.数据库匹配数据,判断用户名和密码是否匹配
        # try:
        #     user = User.objects.get(username=username)
        #     if user.check_password(password) is False:
        #         return HttpResponseForbidden("用户名或密码错误")
        # except User.DoesNotExist:
        #     return HttpResponseForbidden('用户名或密码错误')
        # 用户认证,通过认证返回user 反之返回None
        #django自带登陆认证
        # user = authenticate(request,username=username,password=password)
        # if user is None:
        #     return HttpResponseForbidden("用户名或密码错误")
        #django自带的登陆认证只能判断用户名和密码的匹配关系，如果用户输入的是手机号就不能匹配了，需要自定义
        #1)用户多账号(非官方)
        # qs = User.objects.filter(Q(username=username)|Q(mobile=username))  #qs是QuerySet类型
        # if qs is None:
        #     return HttpResponseForbidden("用户名或密码错误")
        # user = qs[0]
        # if user.check_password(password)  is False:  #模型对象.check_password(原始password) 返回布尔值
        #     return HttpResponseForbidden("用户名或密码错误")
        #2)用户多帐号登录(官方) 自定义用户认证后端
        user = authenticate(request,username=username,password=password)
        if user is None:
            return HttpResponseForbidden("用户名或密码错误")
        #4.状态保持:根据是否勾选remembered确认状态保持的时间
        login(request, user)
        if remembered is None: #如果用户没有勾选记住登录,设置session过期时间为会话结束
            request.session.set_expiry(0)
        #5.为了实现在重定向首页之后，渲染用户名的效果，还需要将用户名传递到首页
        #通过查看前端js代码可以知道，是登录后将用户名存到了cookie中，前端从cookie中取出来的
        # response = redirect(next)
        response = redirect(request.GET.get('next') or '/')
        #这个地方需要设置cookie的过期时间和记住登录的过期时间一致
        response.set_cookie("username",user.username,max_age=SESSION_COOKIE_AGE if remembered == "on" else None)  #需要用户模型对象去取用户名，直接用username可能是电话号
        #6.重定向到来源
        #合并购物车需要在做完状态保持和获取response之后
        merge_cart_cookie_to_redis(request,response)
        return response

class LogoutViews(View):
    """登出视图"""
    def get(self,request):
        #1.删除username的cookie
        response = redirect("/login/")
        response.delete_cookie("username")
        #2.清除状态保持（如果不清除，打开其他页面该用户的信息还会保存）
        logout(request)
        #3.重定向至首页
        return response
#展示用户中心时要注意：如果这时没有用户登录是不应该展示用户中心的，直接重定向至登录页面

# class InfoViews(View):
#     """展示用户中心"""
#     def get(self,request):
#         #1.判断用户是否已经登录,三种方式实现。这个地方不能用None判断，如果没有用户，request.user返回的是匿名用户
#         #1)用isinstance判断用户是否是真实用户
#         # if isinstance(request.user,User):   #包含关系，匿名用户不在用户模型类中，返回布尔值
#         #2)django自带的，判断用户是否登录
#         if request.user.is_authenticated:
#
#             return render(request,'user_center_info.html')
#         else:
#             #这个地方要注意：重定向至用户登录页面，等用户登录完成后应该是重定向至用户中心，而不是原来的首页
#             #所以重定向时应该携带来源地址，登录后直接重定向至来源地
#             return redirect('/login/?next=/info/')
#3)使用Django自带的类
# class InfoViews(LoginRequiredMixin,View):   #在这个类视图中封装了判断用户是否登录和记录来源的功能
#     def get(self,request):
#         return render(request,'user_center_info.html')
#4)使用Django的login_requierd()装饰器

class InfoViews(View):   #在这个装饰器封装了判断用户是否登录和记录来源的功能
    """用户中心"""
    @method_decorator(login_required)
    def get(self,request):
        return render(request,'user_center_info.html')

class EmailViews(LoginRequireView): #在这个类视图中封装了判断用户是否登录和记录来源的功能
    """设置邮箱"""
    #this.host + '/emails/'
    def put(self,request):  #是由axios发来的请求体数据，用request.body获取byte类型数据
        #1.获取邮箱信息
        json_str_byte = request.body
        json_str = json_str_byte.decode()
        json_dict = json.loads(json_str)
        emails = json_dict.get('email')
        #2.校验邮箱格式
        if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', emails):
            return HttpResponseForbidden('邮箱格式有误')
        #3.在用户数据库保存emails字段信息
        master_user = request.user
        if master_user.email == '':  #先判断用户邮箱字段是否为空字符串，这个地方不能用None判断
            master_user.email = emails
            master_user.save()
        #4.发送邮箱验证信息
        #4.1需要通过发送链接来激活邮箱，每个人收到的激活路径应该不同，可以采用加密完成
        verify_url = generate_email_verify_url(master_user)
        #在django.core.mail模块提供了send_mail()来发送邮件。
        # send_mail(subject='主题', message=‘文本内容’, from_email=‘发送邮箱’, recipient_list=‘接收邮箱’列表形式,
        #       fail_silently=False, auth_user=None, auth_password=None,
        #       connection=None, html_message=‘超文本链接’)
        # send_mail("hello","world","美多商城<itcast99@163.com>",[emails])  #发邮箱的过程对响应没影响，可以加入到celery异步中
        send_emails.delay(emails,verify_url)
        #5.响应json数据
        return JsonResponse({'code':RETCODE.OK, 'errmsg': 'OK'})

class VerifyEmailViews(View):
    """激活邮箱"""
    #激活邮箱这里继承类应该是View而不应该是LoginRequireView
    #因为客户在进行邮箱激活时，有可能是在其他浏览器中进行，而其他浏览器此时并没有做状态保存，如果继承LoginRequireView
    #则导致客户在未登录状态下会跳转到登录页面，不符合正常业务逻辑
    #emails/verification/?token=eyJhbGciOiJIUzUxMiIsImlhdCI6MTU3NDYwMDQxMSwiZXhwIjoxNTc0NjA0MDExfQ.eyJpZCI6MiwiZW1haWwiOiIzNzkyOTY1NjVAcXEuY29tIn0.em8oaUtCqHMDl7FZsbdhSXx5c7GbU7doRlztP7wQf7GvVlyfGrShBFrFyO5GipBqMb8fPgEFqoNn2EwQr_BfQg
    def get(self,request):
    #1.获取查询参数的token
        token = request.GET.get("token")
    #2.先对token解密,然后和数据库的用户匹配
        user = get_user_token(token)
    #3.修改邮箱激活字段,并记得保存
        user.email_active = True
        user.save()
    #4.响应
        return redirect('/info/')


class AdressViews(LoginRequireView):
    """新增用户收货地址"""
    def get(self,request):   #在展示用户收货地址时将地址渲染到页面 let addresses = {{ addresses | safe }} let default_address_id = {{ default_address_id }};
        user = request.user
        addresses = []
        #获取当前用户未被逻辑删除的所有收货地址
        address_qs = Address.objects.filter(user=user,is_deleted=False)
        for add_address in address_qs:
            address_dict = {
                'id':add_address.id,
                'title': add_address.title,
                'receiver': add_address.reciver,
                'province_id': add_address.province_id,
                'province': add_address.province.name,  # 根据外键可以得到 一的一方对象 =多对象.关联属性名
                'city_id': add_address.city_id,
                'city': add_address.city.name,
                'district_id': add_address.district_id,
                'district': add_address.district.name,
                'place': add_address.place,
                'mobile': add_address.mobile,
                'tel': add_address.tel,
                'email': add_address.email,
            }
            addresses.append(address_dict)
        context = {
            'addresses':addresses,
            'default_address_id': user.default_address_id,       #当前用户的默认收货地址
        }
        return render(request,'user_center_site.html',context)


class AddressCreateView(LoginRequireView):
    """新增收货地址"""
    #"POST /addresses/create/ HTTP/1.1" 404 4517
    def post(self,request):
        #1.接收前端的数据  axios发来的post请求，请求体的内容用request.body接收  axios.post(url, this.form_address
        json_str_byte = request.body  #接收到的是byte字符串类型
        json_str = json_str_byte.decode()
        json_dict = json.loads(json_str)  #转成json字典格式
        # #form_address: {
        #     title: '',
        #     receiver: '',
        #     province_id: '',
        #     city_id: '',
        #     district_id: '',
        #     place: '',
        #     mobile: '',
        #     tel: '',
        #     email: '',
        # },
        title = json_dict.get('title')
        reciver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.pop('tel')
        email = json_dict.pop('email')
        user = request.user   #通过请求获取用户对象
        #2.校验  一般从前端传来的数据必传参数一般都是有的，做校验是为了防止其他方式的恶意请求
        #2.1检验必传参数是否都有
        if all(json_dict.values()) is False:
            return HttpResponseForbidden("缺少必传参数")
        #2.2检查手机号码是否符合要求
        if re.match(r'1[3-9]\d{9}',mobile) is None:
            return HttpResponseForbidden("手机号码格式不正确")
        if tel:
            if not re.match(r'^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$', tel):
                return HttpResponseForbidden('参数tel有误')
        if email:
            if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
                return HttpResponseForbidden('参数email有误')
        #3.新增收货地址
        #3.1创建用户地址模型对象
        add_address = Address(

            user = user,
            title = title,
            reciver = reciver,
            province_id = province_id,
            city_id = city_id,
            district_id = district_id,
            place = place,
            mobile = mobile,
            tel = tel,
            email = email,
        )
        try:  #不确定省市区id是否和外键相同
            add_address.save()
        except:
            return  HttpResponseForbidden("禁止访问")
        # 3.2如果用户还没有默认地址,就把当前新增的地址设置为它的默认地址
        if user.default_address is None:
            user.default_address = add_address
            user.save()
        #4.响应json
        #4.1 构造json数据格式
        address = {
            'id': add_address.id,
            'title':add_address.title,
            'receiver':add_address.reciver,
            'province_id':add_address.province_id,
            'province':add_address.province.name,    #根据外键可以得到 一的一方对象 =多对象.关联属性名
            'city_id':add_address.city_id,
            'city':add_address.city.name,
            'district_id':add_address.district_id,
            'district':add_address.district.name,
            'place':add_address.place,
            'mobile':add_address.mobile,
            'tel':add_address.tel,
            'email':add_address.email,
        }
        #4.2 响应json数据
        return JsonResponse({'code':RETCODE.OK,'errmsg': 'OK','address':address})

class SaveTitleView(LoginRequireView):
    """修改收货地址标题"""
    def put(self,request,index):   #/addresses/5/title/
        #1.获取数据
        addresses_index = index
        json_str = json.loads(request.body.decode())
        input_title = json_str.get('title')
        #2.校验
        #检查是否为空
        if input_title is None:
            return HttpResponseForbidden("修改的标题不能为空")
        #3.修改并保存
        try:
            # 获取当前的address对象
            modify_address = Address.objects.get(id=addresses_index)
            #修改title属性
            modify_address.title = input_title
            #保存
            modify_address.save()
        except:
            return HttpResponseForbidden("id值不能查找到")
        #4.响应json数据
        return JsonResponse({'code':RETCODE.OK,'errmsg': 'OK'})


class ModifyAddressView(LoginRequireView):
    """修改或删除收货地址"""
    def put(self,request,index_id):
        """修改收货地址"""
        # var url = this.host + '/addresses/' + this.addresses[this.editing_address_index].id + '/';
        #1.获取数据
        id = index_id   #该地址在address表中的id字段,得到的是字符串
        json_str = json.loads(request.body.decode())
        address_id = json_str.get('id')  #这个是int类型
        address_title = json_str.get('title')
        address_receiver = json_str.get('receiver')
        address_province_id = json_str.get('province_id')
        address_province = json_str.get('province')
        address_city_id = json_str.get('city_id')
        address_city = json_str.get('city')
        address_district_id = json_str.get('district_id')
        address_district = json_str.get('district')
        address_place = json_str.get('place')
        address_mobile = json_str.get('mobile')
        address_tel = json_str.pop('tel')
        address_email = json_str.pop('email')
        #2.校验
        #校验必传参数有没有
        #校验地址的id值是否一致
        #校验手机号码
        #校验电话号码
        #校验邮箱
        if all(json_str.values()) is False:
            return HttpResponseForbidden("缺少必传参数")
        if int(id) != address_id:
            return HttpResponseForbidden("id值不匹配")
        if re.match(r'1[3-9]\d{9}',address_mobile) is None:
            return HttpResponseForbidden("手机号码格式不正确")
        if address_tel:
            if not re.match(r'^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$', address_tel):
                return HttpResponseForbidden('参数tel有误')
        if address_email:
            if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', address_email):
                return HttpResponseForbidden('参数email有误')
        #<class 'dict'>: {'id': 3, 'title': 'EEWERW', 'receiver': 'EEWERW', 'province_id': 110000,
        # 'province': '北京市', 'city_id': 110100, 'city': '北京市',
        # 'district_id': 110101, 'district': '东城区', 'place': 'WREWRFFVsssss', 'mobile': '13434543456',
        # 'tel': '', 'email': ''}
        #3.业务逻辑处理
        #修改并保存收货地址
        try:
            address = Address.objects.get(id=id)
            address.title = address_title
            address.receiver = address_receiver
            address.province_id = address_province_id
            address.city_id = address_city_id
            address.district_id = address_district_id
            address.place = address_place
            address.mobile = address_mobile
            address.tel = address_tel
            address.email = address_email
            address.save()
        except:
            return HttpResponseForbidden("参数有误")

        #4.响应json数据
        #构造json数据格式
        address = {
            'id':address_id,
            'title':address_title,
            'receiver':address_receiver,
            'province_id':address_province_id,
            'province':address_province,
            'city_id':address_city_id,
            'city':address_city,
            'district_id':address_district_id,
            'district':address_district,
            'place':address_place,
            'mobile':address_mobile,
            'tel':address_tel,
            'email':address_email,
        }

        return JsonResponse({'code':RETCODE.OK,'errmsg': 'OK','address':address})


    def delete(self,request,index_id):
        """删除收货地址"""
        address = Address.objects.get(id=index_id)
        address.is_deleted = True
        address.save()
        return JsonResponse({'code':0})

class ModifyPasswordView(LoginRequireView):
    """修改密码"""
    def get(self,request):
        #1.接收请求
        #2.校验数据
        #3.业务逻辑处理
        #4.响应渲染页面
        return render(request,'user_center_pass.html')
    def post(self,request):
        # 1.接收请求
        user = request.user  #接收请求用户信息
        old_pwd = request.POST.get('old_pwd')
        new_pwd = request.POST.get('new_pwd')
        new_cpwd = request.POST.get('new_cpwd')
        # 2.校验数据
        if all([old_pwd,new_pwd,new_cpwd]) is False:
            return HttpResponseForbidden("缺少必传参数")
        if user.check_password(old_pwd) is False:
            return HttpResponseForbidden("原密码错误")
        if re.match(r'^[0-9A-Za-z]{8,20}$',new_pwd) is None:
            return HttpResponseForbidden("密码格式不正确")
        if user.check_password(new_pwd) is True:
            return HttpResponseForbidden('密码设置错误')
        if new_pwd != new_cpwd:
            return HttpResponseForbidden("两次输入密码不匹配")
        # 3.业务逻辑处理
        #修改原密码，并进行加密处理
        user.set_password(new_pwd)
        user.save()
        # 4.响应渲染页面
        #最后应该重定向至登录页面
        return redirect('/login/')

class DefaultAddressView(LoginRequireView):
    """修改默认地址"""
    def put(self,request,id):
        #1.接收请求数据
        user = request.user
        #2.校验
        if id is None:
            return HttpResponseForbidden("缺少必传参数")
        #3.设置为默认地址
        user.default_address_id = id
        user.save()
        #4.响应json数据
        return JsonResponse({'code':0})

class HistoriesView(View):
    """用户的历史浏览记录页面"""
    def post(self,request):    #这个请求是在商品详情页发起的，这个时候匿名用户同样可以访问，继承类只能是View
        """保存登录用户的浏览记录"""
        #1.获取数据
        json_dict = json.loads(request.body.decode())   #request.body返回bytes类型
        sku_id = json_dict.get('sku_id')  #获取当前的sku商品id
        user = request.user  #获取当前用户对象
        #2.校验
        #检验用户是否为匿名用户
        if not user.is_authenticated:
            return JsonResponse({'code':RETCODE.SESSIONERR,'errmsg':'用户未登录'})
        #检查该id是否存在
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('id不存在')
        #3.业务逻辑处理
        #保存登录用户的浏览记录到redis
        #3.1 创建用户浏览记录redis数据库
        redis_cnn = get_redis_connection('histories')
        #redis管道技术优化性能
        pl = redis_cnn.pipeline()
        #3.2 将每个用户的浏览记录以列表形式（列表有顺序）存放到redis ,每次向最前面添加数据
        key = 'user_%d' %user.id
        # 先去重
        pl.lrem(key, 0, sku_id)
        #将新增书据添加到最前面
        pl.lpush(key,sku_id)

        #3.3 截取前五个数据进行展示
        pl.ltrim(key,0,4)
        #3.4执行管道
        pl.execute()
        #4.响应json数据  没有需要响应的
        return JsonResponse({'code':RETCODE.OK,'errmsg':'OK'})
    def get(self,request):
        """获取用户浏览历史记录页面"""
        #1.获取数据
        user = request.user   #获取当前用户对象
        #2.校验数据
        if not user.is_authenticated:
            return HttpResponseForbidden('用户不存在')
        #3.业务逻辑处理
        #3.1获取用户对象的历史浏览数据
        redis_cnn = get_redis_connection('histories')
        sku_list = redis_cnn.lrange('user_%d' %user.id,0,-1)
        skus = []
        for sku_id in sku_list:
            try:
                sku = SKU.objects.get(id=sku_id)
            except:
                return HttpResponseForbidden('id不存在')
            skus_dict = {
                'id':sku.id,
                'name':sku.name,
                'price':sku.price,
                'default_image_url':sku.default_image.url,
                }
            skus.append(skus_dict)
        #4.响应json数据，skus
        return JsonResponse({'code':RETCODE.OK,'errmsg':'OK','skus':skus})