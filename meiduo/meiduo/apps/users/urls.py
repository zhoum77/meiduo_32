from django.conf.urls import url
from .views import RegisterViews,CheckUsername,CheckMobile,LoginViews,LogoutViews,InfoViews,EmailViews,VerifyEmailViews,AdressViews,AddressCreateView,SaveTitleView,ModifyAddressView,ModifyPasswordView,DefaultAddressView,HistoriesView
from django.contrib.auth.decorators import login_required
urlpatterns = [
    #用户注册
    url(r'^register/$',RegisterViews.as_view()),
    #校验用户名
    url(r'^usernames/(?P<username>[a-zA-Z0-9_-]{5,20})/count/$',CheckUsername.as_view()),
    #校验手机号
    url(r'^mobiles/(?P<mobile>1[345789]\d{9})/count/$',CheckMobile.as_view()),
    #用户登录
    url(r'^login/$',LoginViews.as_view()),
    #用户退出登录
    url(r'^logout/$',LogoutViews.as_view()),
    # url(r'^info/$',login_required(InfoViews.as_view())),
    #用户中心
    url(r'^info/$',InfoViews.as_view()),
    #发送邮件
    url(r'^emails/$',EmailViews.as_view()),
    #激活邮箱
    url(r'^emails/verification/$',VerifyEmailViews.as_view()),
    #收货地址
    url(r'^addresses/$',AdressViews.as_view()),
    #新增收货地址
    url(r'^addresses/create/$',AddressCreateView.as_view()),
    #修改收货地址标题
    url(r'^addresses/(?P<index>\d+)/title/$',SaveTitleView.as_view()),
    #修改或删除收货地址
    url(r'^addresses/(?P<index_id>\d+)/$',ModifyAddressView.as_view()),
    #修改密码
    url(r'^password/$',ModifyPasswordView.as_view()),
    #设置默认地址
    url(r'^addresses/(?P<id>\d+)/default/$',DefaultAddressView.as_view()),
    # 用户浏览历史记录
    url(r'^browse_histories/$', HistoriesView.as_view()),
]
#this.host + '/mobiles/' + this.mobile + '/count/'
# var url = this.host + '/addresses/' + this.addresses[this.editing_address_index].id + '/';
#   addresses/' + this.addresses[index].id + '/';
#password/