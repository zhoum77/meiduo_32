from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.conf import settings

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,BadData
import re
from .models import User

def get_user_by_account(account):
    """
    根据account查询用户
    :param account: 用户名或者手机号
    :return: user
    """
    # 这里的get方法如果没有符合条件的，会报错，需要try一下
    try:
        if re.match(r'^1[3-9]\d{9}$',account):

            user = User.objects.get(mobile=account)
        else:
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        return None
    else:
        return user


class UsernameMobileAuthBackend(ModelBackend):
    #自定义用户认证后端
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
        重写认证方法，实现多帐号登录
        :param request: 请求对象
        :param username: 用户名
        :param password: 密码
        :param kwargs: 其它参数
        :return: user
        """
        #通过get_user_by_account函数判断传入的username是用户名还是手机号
        #判断完之后返回一个user对象
        user = get_user_by_account(username)
        #返回的user可能为None，也可能是一个用户模型对象，需要判断
        #如果是一个真实的用户模型对象还需要将其密码与传入的密码匹配，匹配成功后才能返回user
        # if user is not None:
        #     if user.check_password(password):
        #         return user
        # else:
        #     return None
        #这个地方注意：用户帐号在进行登录时需要区分是商城前台请求，还是后台请求。
        #通过传过来的request判断，前台传来的是request对象，而后台在登录认证时并没有传入request
        if not request:  #如果请求的是后台，判断is_stuff字段是否为True
            if user.is_staff:
                return user
        else:
            if user and user.check_password(password):
                return user


def generate_email_verify_url(user):  #同openid加密方法
    """
    生成邮箱激活url
    :param user: 那个用户要生成激活url
    :return: 邮箱激活url
    """
    id = user.id
    email = user.email
    data = {
        'id':id,
        'email':email,
    }
    #1.创建Serializer实例化对象
    serializer = Serializer(settings.SECRET_KEY,expires_in=3600) #自定义过期时间为1小时
    #2.调用实例化对象的dumps方法,得到的是字符串byte类型
    token = serializer.dumps(data).decode()
    verify_url = settings.EMAIL_VERIFY_URL + '?token='+ token
    return verify_url


def get_user_token(token):
    """对token进行解密,并查询出对应的user"""
    # 1.创建Serializer实例化对象
    serializer = Serializer(settings.SECRET_KEY, expires_in=3600)  # 内容与加密时一致
    # 2.调用实例化对象的loads方法,得到的是字典类型
    try:
        data = serializer.loads(token)
        user_id = data.get("id")
        user_email = data.get('email')
        user = User.objects.get(id=user_id,email=user_email)
        return user
    except BadData:
        return None
