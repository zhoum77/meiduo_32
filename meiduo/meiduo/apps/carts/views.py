from django.shortcuts import render
from django.views import View
from django.http import JsonResponse,HttpResponseForbidden
from django_redis import get_redis_connection
from meiduo.utils.response_code import RETCODE
from goods.models import SKU
import json,pickle,base64
# Create your views here.
class GoodsCartsView(View):
    """商品购物车"""
    def post(self,request):
        """购物车添加，包括登录用户和未登录用户"""
        #1.获取数据
        json_dict = json.loads(request.body.decode())
        sku_id = json_dict.get('sku_id')
        count = json_dict.get('count')
        selected = json_dict.get('selected',True)  #加入购物车默认勾选
        #2.校验数据
        #该id商品是否存在
        try:
            sku = SKU.objects.get(id=sku_id,is_launched=True)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('商品不存在')
        #商品数量是否为整数
        try:
            count = int(count)
        except TypeError:
            return HttpResponseForbidden('数量类型错误')
        #勾选状态时否为bool值
        if isinstance(selected,bool) is False:
            return HttpResponseForbidden('参数错误')
        #3.业务逻辑处理
        # 要设置cookie先获取响应对象
        response = JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK'})
        #判断用户信息
        user = request.user
        #如果是登录用户，将信息存入redis
        if user.is_authenticated:
            redis_conn = get_redis_connection('carts')
            #这个地方应该用hincrby可以直接判断哈希中是否已有value，有的话直接增加count，而不是hset，这是覆盖
            redis_conn.hincrby('cart_user%s' %user.id,sku_id,count)   #哈希表存放商品及对应数量 name = 'carts_user1' {key:value} = {sku_id:count}
            if selected:
                redis_conn.sadd('selected_user%s' %user.id,sku_id)   #如果勾选，将勾选的商品id存入用户集合  name = 'selected_user1' value=(sku_id)

        #如果是非登录用户，将信息存入cookie
        else:
            #1.先判断一下cookie是否已经有数据
            carts = request.COOKIES.get('carts')
            # 2.1cookie中没有购物车数据
            if carts is None:
                #cookie中存储key：value ,value是字符串格式，需要先将数据转成字符串格式
                carts_dict = {}
                # #将数据包成字典
                # carts_dict[sku_id] = {'count':count,'selected':selected}
                # #将字典转成字符串，b'unicode编码
                # carts_uni = pickle.dumps(carts_dict)
                # #unicode --> b'普通字符串
                # carts_byte = base64.b64encode(carts_uni)
                # #b'普通字符串 --> 普通字符串
                # carts = carts_byte.decode()

            else:
                #2.2cookie中已有购物数据，则为追加数据
                #将carts字符串转成字典后追加数据
                #str --> b'str -->b'unicode -->dict
                carts_dict = pickle.loads(base64.b64decode(carts.encode()))
                #判断加入购物车的商品是否已经在购物车cookie数据里，在就直接追加数据
                if sku_id in carts_dict:
                    count += carts_dict[sku_id]['count']

                #原cookie中没有该商品记录，则新增
            carts_dict[sku_id] = {'count':count,'selected':selected}
                #将字典转成字符串存回字符串
            carts = base64.b64encode(pickle.dumps(carts_dict)).decode()
            response.set_cookie('carts', carts)
        #4.响应json
        return response

    def get(self,request):
        """购物车展示"""
        #1.获取数据
        #2.校验数据
        #3.业务逻辑处理
        #判断该请求是登录用户还是非登录用户发起的
        user = request.user
        if user.is_authenticated:
            #登录用户去redis获取购物车数据
            redis_cnn = get_redis_connection('carts')
            carts_dict = redis_cnn.hgetall('cart_user%s' %user.id)
            selected_ids = redis_cnn.smembers('selected_user%s' %user.id)
            cart_skus = []
            if carts_dict:

                for sku_id,count in carts_dict.items():
                    try:
                        sku = SKU.objects.get(id=sku_id)
                    except SKU.DoesNotExist:
                        return HttpResponseForbidden('id不存在')
                    count = int(count)
                    carts = {
                                'id':sku.id,
                                'name':sku.name,
                                'price':str(sku.price),
                                'selected':str(sku_id in selected_ids) ,
                                'default_image_url':sku.default_image.url,
                                'count':count,
                                'amount':str(sku.price*count),
                            }

                    cart_skus.append(carts)
            context = {
                'cart_skus': cart_skus,
            }

            return render(request, 'cart.html', context)


        else:
            #非登录用户去cookie获取购物车数据
            cart_str = request.COOKIES.get('carts')
            #字符串转字典
            if not cart_str:
                return render(request, 'cart.html')
            else:
                cart_dict = pickle.loads(base64.b64decode(cart_str.encode()))
                cart_skus = []
                for sku_id,values in cart_dict.items():
                    count = int(values['count'])
                    selected = str(values['selected'])
                    sku = SKU.objects.get(id=sku_id)
                    carts = {
                        'id': sku.id,
                        'name': sku.name,
                        'price': str(sku.price),
                        'selected': selected,
                        'default_image_url': sku.default_image.url,
                        'count': count,
                        'amount': str(sku.price * count),
                    }

                    cart_skus.append(carts)
                context = {'cart_skus': cart_skus}

                return render(request, 'cart.html', context)


    def put(self,request):
        """修改购物车信息"""
        #1.获取数据
        json_dict = json.loads(request.body.decode())
        sku_id = json_dict.get('sku_id')   #int
        count = json_dict.get('count')   #int
        selected = json_dict.get('selected')  #bool
        #2.校验
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('id不存在')
        try:
            count = int(count)
        except TypeError:
            return HttpResponseForbidden('参数类型错误')
        if isinstance(selected,bool) is False:
            return HttpResponseForbidden('参数类型错误')

        #3.业务逻辑处理
        cart_sku = {
            'id': sku.id,
            'name': sku.name,
            'price': str(sku.price),
            'selected': selected,
            'default_image_url': sku.default_image.url,
            'count': count,
            'amount': str(sku.price * count),
        }

        response = JsonResponse({'code':RETCODE.OK,'errmsg':'ok','cart_sku':cart_sku})
        #区分登录用户和非登录用户
        user = request.user
        #3.1如果是登录用户，获取redis信息，修改
        if user.is_authenticated:
            redis_cnn = get_redis_connection('carts')
            # 修改hash中的数据 直接覆盖旧数据
            redis_cnn.hset('cart_user%s' %user.id,sku_id,count)
            #修改勾选状态
            if selected:
                redis_cnn.sadd('selected_user%s' %user.id,sku_id)
            else:
                redis_cnn.srem('selected_user%s' %user.id,sku_id)
        #3.2 非登录用户，获取cookie的信息，修改
        else:
            #获取cookie中的字符串
            carts = request.COOKIES.get('carts')
            #字符串转字典
            carts_dict = pickle.loads(base64.b64decode(carts.encode()))
            # 修改数据
            carts_dict[sku_id] = {'count':count,'selected':selected}
            #将修改完的数据以字符串形式存回cookie
            carts_str = base64.b64encode(pickle.dumps(carts_dict)).decode()
            response.set_cookie('carts',carts_str)

        #4.响应json数据
        return response


    def delete(self,request):
        """删除购物车商品"""
        #1.获取数据
        json_dict = json.loads(request.body.decode())
        sku_id = json_dict.get('sku_id')
        #2.校验
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return HttpResponseForbidden('id不存在')
        #3.删除数据
        #判断是登录用户还是非登录用户
        response = JsonResponse({'code':RETCODE.OK,'errmsg':'ok'})
        user = request.user
        #如果是登录用户，操作redis删除数据
        if user.is_authenticated:
            redis_conn = get_redis_connection('carts')
            redis_conn.hdel('cart_user%s' %user.id,sku_id)
            redis_conn.srem('selected_user%s' %user.id,sku_id)

        #如果是非登录用户
        else:
            carts_str = request.COOKIES.get('carts')
            if carts_str is None:
                return HttpResponseForbidden('数据异常')
            carts_dict = pickle.loads(base64.b64decode(carts_str.encode()))
            if sku_id in carts_dict:
                del carts_dict[sku_id]
            else:
                return JsonResponse({'code': RETCODE.DBERR, 'errmsg': '购物车内无此商品'})
            if not carts_dict:
                response.delete_cookie('carts')
                return response
            carts = base64.b64encode(pickle.dumps(carts_dict)).decode()
            response.set_cookie('carts',carts)
        #4.响应
        return response



class CartSelectedAllView(View):
    """全选/取消全选"""
    def put(self,request):
        #1.接收数据
        json_dict = json.loads(request.body.decode())
        selected = json_dict.get('selected')
        #2.校验数据
        if not isinstance(selected,bool):
            return HttpResponseForbidden('参数类型错误')
        #3.业务逻辑处理
        user = request.user
        response = JsonResponse({'code':RETCODE.OK,'errmsg':'ok'})
        #判断selected是false还是true，将selected的vaule改掉
        #3.1登录用户
        if user.is_authenticated:
            redis_conn = get_redis_connection('carts')
            if selected:
                #获取该用户购物车所有的商品id
                sku_ids = redis_conn.hkeys('cart_user%s' %user.id)
                #将商品id添加到set集合中，代表所有商品全被选中
                redis_conn.sadd('selected_user%s' %user.id,*sku_ids)
            else:
                redis_conn.delete('selected_user%s' %user.id)
        #3.2非登录用户
        else:
            carts = request.COOKIES.get('carts')
            carts_dict = pickle.loads(base64.b64decode(carts.encode()))
            for sku in carts_dict.values():
                sku['selected'] = selected
            #将字典转成字符串，存回cookie
            carts_str = base64.b64encode(pickle.dumps(carts_dict)).decode()
            response.set_cookie('carts',carts_str)
        #4.响应json
        return response

class CartSimpleView(View):
    """展示简单版购物车数据"""
    def get(self,request):
        #1接收
        #2校验数据
        #3业务逻辑处理
        #3.1区分是登录用户还是非登录用户
        user = request.user
        #登录用户获取redis数据
        if user.is_authenticated:
            redis_conn = get_redis_connection('carts')
            redis_carts = redis_conn.hgetall('cart_user%s' %user.id)
            redis_selecteds = redis_conn.smembers('selected_user%s' %user.id)
            #将登录用户redis数据包装成未登录用户cookie的数据格式，方便后面统一进行处理
            #{‘sku_id’:{'count':count,'selected':selected}}
            #先定义一个大字典
            carts_dict = {}
            for sku_id_byte,count_byte in redis_carts.items():
                carts_dict[int(sku_id_byte)] = {'count':int(count_byte),'selected':sku_id_byte in redis_selecteds}   #这个地方selected取值很巧秒

        #非登录用户获取cookie数据
        else:
            carts_str = request.COOKIES.get('carts')
            if carts_str is None:
                return JsonResponse({'code': RETCODE.NODATAERR, 'errmsg': '没有购物车数据'})
            #将cookie中获取到的字符串转换成字典
            carts_dict = pickle.loads(base64.b64decode(carts_str.encode()))
        cart_skus = []
        for sku_id, cart_dict in carts_dict.items():
            sku = SKU.objects.get(id=sku_id)
            count = cart_dict['count']
            # 4响应json数据cart_skus
            cart_sku = {
                'id': sku.id,
                'name': sku.name,
                'default_image_url': sku.default_image.url,
                'count': count,
            }
            cart_skus.append(cart_sku)

        return JsonResponse({'code':RETCODE.OK,'errmsg':'ok','cart_skus':cart_skus})


