import pickle,base64
from django_redis import get_redis_connection
#该功能在登录完成时触发，将未登录用户cookie中的数据转化成登录用户redis数据，
#redis中没有的商品直接新增，有的商品以cookie为准
def merge_cart_cookie_to_redis(request,response):
    """购物车合并"""
    #获取未登录用户cookie购物车数据
    carts_str = request.COOKIES.get('carts')
    # 判断cookie中是否有数据
    if carts_str is None:
        return
    carts_dict = pickle.loads(base64.b64decode(carts_str.encode()))

    #获取登录用户的redis购物车数据
    user = request.user
    redis_conn = get_redis_connection('carts')
    redis_carts = redis_conn.hgetall('cart_user%s'%user.id)
    redis_selecteds = redis_conn.smembers('selected_user%s'%user.id)
    #判断cookie数据和redis数据

    for sku_id,cart in carts_dict.items():
        redis_conn.hset('cart_user%s' % user.id, sku_id, cart['count'])
        if cart['selected']:
            redis_conn.sadd('selected_user%s' % user.id, sku_id)
        else:
            redis_conn.srem('selected_user%s' % user.id, sku_id)

    #合并后删除cookie购物车数据
    response.delete_cookie('carts')



