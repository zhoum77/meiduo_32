from django.conf.urls import url
from .views import GoodsCartsView,CartSelectedAllView,CartSimpleView
urlpatterns = [
    #商品购物车添加/展示/修改/删除
    url(r'^carts/$',GoodsCartsView.as_view()),
    #商品全选操作
    url(r'^carts/selection/$',CartSelectedAllView.as_view()),
    #展示简单版购物车数据
    url(r'^carts/simple/$',CartSimpleView.as_view()),
]