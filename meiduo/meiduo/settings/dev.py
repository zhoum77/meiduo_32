#开发阶段的配置
"""
Django settings for meiduo project.

Generated by 'django-admin startproject' using Django 1.11.11.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
import sys
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'm2%u^o!fv+nfil^=y$-5qj+(@7whf*+nz!wzyb2^89%t=!q0wr'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['www.meiduo.site','127.0.0.1']   #添加允许访问的域名


# Application definition
#添加导包路径,将apps目录作为导包路径传入
sys.path.insert(0,os.path.join(BASE_DIR,'apps'))
# print(sys.path)   所有的导包路径
#'/home/python/Desktop/meiduo_32/meiduo',
#'/home/python/Desktop/meiduo_32/meiduo/meiduo/apps'
INSTALLED_APPS = [
    'corsheaders',         #跨域访问处理拓展
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'users.apps.UsersConfig', #注册应用
    'oauth.apps.OauthConfig',
    'areas.apps.AreasConfig',
    'contents.apps.ContentsConfig',
    'goods.apps.GoodsConfig',
    'orders.apps.OrdersConfig',
    'payment.apps.PaymentConfig',
    'weibo.apps.WeiboConfig',
]
from corsheaders.middleware import CorsMiddleware
MIDDLEWARE = [
    # 所有的options请求都会经过该中间件处理
    # 这个中间件帮我我们回应浏览器的OPTIONS询问请求
    'corsheaders.middleware.CorsMiddleware',   #设置跨域检查请求头信息
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'meiduo.urls'

# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]
#配置jinja2的模板引擎
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',  # jinja2模板引擎
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            # 补充Jinja2模板引擎环境
            'environment': 'meiduo.utils.jinja2_env.jinja2_environment',
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],

        },
    },
]

WSGI_APPLICATION = 'meiduo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }
#配置mysql数据库
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql', # 数据库引擎
#         'HOST': '127.0.0.1', # 数据库主机
#         'PORT': 3306, # 数据库端口
#         'USER': 'root', # 数据库用户名
#         'PASSWORD': 'mysql', # 数据库用户密码
#         'NAME': 'meiduo' # 数据库名字
#     },
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # 数据库引擎
        'HOST': '127.0.0.1', # 数据库主机
        'PORT': 3306, # 数据库端口
        'USER': 'root', # 数据库用户名
        'PASSWORD': 'mysql', # 数据库用户密码
        'NAME': 'meiduo_mall_db' # 数据库名字
    },
}
#配置redis缓存
CACHES = {
    "default": { # 默认
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "session": { # session
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "verify_code": {  # 验证码        配置验证码缓存
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "histories": {  # 用户商品浏览记录
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/3",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "carts": {  # 登录用户购物车信息
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/4",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
}
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "session"

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#配置工程日志
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,  # 是否禁用已经存在的日志器
    'formatters': {  # 日志信息显示的格式
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(module)s %(lineno)d %(message)s'
        },
    },
    'filters': {  # 对日志进行过滤
        'require_debug_true': {  # django在debug模式下才输出日志
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {  # 日志处理方法
        'console': {  # 向终端中输出日志
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {  # 向文件中输出日志
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.dirname(BASE_DIR), 'logs/meiduo.log'),  # 日志文件的位置
            'maxBytes': 300 * 1024 * 1024,
            'backupCount': 10,
            'formatter': 'verbose'
        },
    },
    'loggers': {  # 日志器
        'django': {  # 定义了一个名为django的日志器
            'handlers': ['console', 'file'],  # 可以同时向终端与文件中输出日志
            'propagate': True,  # 是否继续传递日志信息
            'level': 'INFO',  # 日志器接收的最低日志级别
        },
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

## django工程默认指定的时区简称
# TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Shanghai'   #这个是django工程显示的页面时间，和数据库中的关于日期的数据无关

# from django.utils.timezone import now # utc时间
# from datetime import datetime
# datetime.now()# 本地

USE_I18N = True

USE_L10N = True

#开启django"时区"功能：保存到数据库中的关于日期的数据，会同一转化成UTC（0时区）时间
USE_TZ = True
# USE_TZ = False    #设置成False，数据库中的关于日期的数据就会设置成本地时间，不会转化成UTC时间了

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
#指定静态文件加载路径
STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

# 修改Django认证模块的模型类
# Invalid model reference 'users.models.User'. String model references must be of the form 'app_label.ModelName'.
# AUTH_USER_MODEL = '应用名.模型名'
# Dependency on app with no migrations: users  # 你应用迁移了
# AUTH_USER_MODEL refers to model 'users.User' that has not been installed  # 要注册users应用
AUTH_USER_MODEL = 'users.User'
#修改django的用户认证后端
# 指定自定义的用户认证后端
AUTHENTICATION_BACKENDS = ['users.utils.UsernameMobileAuthBackend']
#修改用户登陆成功后的重定向路径。
LOGIN_URL = '/login/'

#.配置邮件服务器
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'  # 指定邮件后端
EMAIL_HOST = 'smtp.163.com'  # 发邮件主机
EMAIL_PORT = 25  # 发邮件端口
EMAIL_HOST_USER = 'itcast99@163.com'  # 授权的邮箱
EMAIL_HOST_PASSWORD = 'python99'  # 邮箱授权时获得的密码，非注册登录密码
EMAIL_FROM = '美多商城=<itcast99@163.com>'  # 发件人抬头

# 邮箱验证链接
EMAIL_VERIFY_URL = 'http://www.meiduo.site:8000/emails/verification/'

#设置fastdfs存储下载位置
# MEDIA_URL = 'http://image.meiduo.site:8888/'
# MEDIA_URL = 'http://192.168.103.210:8888/'
#1）可以通过修改hosts和设置fastdfs显示图片
#2）自定义Django文件存储类，实现图片的存储和下载
# 修改Django默认文件存储类
DEFAULT_FILE_STORAGE = 'meiduo.utils.fastdfs.fastdfs_storage.FastDFSStorage'

# 支付宝
ALIPAY_APPID = '2016101700704941'
ALIPAY_DEBUG = True  # 表示是沙箱环境还是真实支付环境
ALIPAY_URL = 'https://openapi.alipaydev.com/gateway.do'
ALIPAY_RETURN_URL = 'http://www.meiduo.site:8000/payment/status/'

#微博
APP_KEY = 3305669385
APP_SECRET = '74c7bea69d5fc64f5c3b80c802325276'
REDIRECT_URL = 'http://www.meiduo.site:8000/sina_callback'
from corsheaders.conf import settings
#设置CORS白名单
CORS_ORIGIN_WHITELIST = ('http://127.0.0.1:8080',)
CORS_ALLOW_CREDENTIALS = True  # 允许携带cookie
# REST_FRAMEWORK框架配置
REST_FRAMEWORK = {
    # jwt身份认证后端
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',  #第一种jwt认证
        'rest_framework.authentication.SessionAuthentication',  #第二种session认证
        'rest_framework.authentication.BasicAuthentication',   #django基本认证
    ),
}
import datetime
# jwt拓展的配置项
JWT_AUTH = {
    # 签发的token的有效期： timedelta对象表示一天
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    #指明自定义jwt拓展构建响应数据
    'JWT_RESPONSE_PAYLOAD_HANDLER':'meiduo_admin.defined_jwt_payload_handler.defined_jwt_payload_handler',
}

# 记录fdfs客户端配置文件路径,非导包路径，只是文件查找路径
FDFS_CONF_PATH = os.path.join(BASE_DIR,'utils/fastdfs/client.conf')