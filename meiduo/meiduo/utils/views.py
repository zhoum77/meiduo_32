from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
#在这个类视图中封装了判断用户是否登录和记录来源的功能
class LoginRequireView(LoginRequiredMixin,View):
    pass