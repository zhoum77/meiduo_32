from django.core.files.storage import Storage
from .fastdfs_fileupload_handler import fdfs_fileupload_handler
class FastDFSStorage(Storage):
    """自定义文件存储类"""
    def _open(self,name,mode='rb'):
        """
        当要打开某个文件时，会自动调用此方法
        :param name: 打开的文件名
        :param mode: 打开模式默认为二进制只读
        :return: 文件对象
        """
        pass
    def _save(self,name,content):
        """
        当上传图片时就会自动调用此方法
        :param name: 要上传的图片名
        :param content: 图片的byte数据,文件对象file object
        :return: file_id
        """

        file_id = fdfs_fileupload_handler(content)
        return file_id
    def exists(self, name):
        """
        当上传图片时，会自动调用此方法判断该图片是否已经存在
        :param name: 图片名
        :return: False 不存在，可以上传;Ture已经存在，不上传
        """
        return False

    def url(self, name):
        """
        当要下载图片，当触发image.url时就会调用此方法
        :param name: file_id
        :return: 完整图片下载路径 ‘http://image.meiduo.site:8888/’ +name
        """
        return 'http://image.meiduo.site:8888/' +name