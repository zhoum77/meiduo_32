from fdfs_client.client import Fdfs_client
from rest_framework import serializers
from django.conf import settings
def fdfs_fileupload_handler(image_file,conf_path=settings.FDFS_CONF_PATH):
    # 2.调用fdfs存储类，将图片上传
    #这里要注意：conf_path应该是导包路径下的相对路径
    #断言的使用：assert 为假 运行逗号后的内容
    assert image_file and conf_path,'您必须传入配置文件和文件数据！'
    conn = Fdfs_client(conf_path)  # 创建fdsf客户端（指定配置文件）
    content = image_file.read()  # 获取图片文件中的内容，bytes类型
    ret = conn.upload_by_buffer(filebuffer=content)  # filebuffer代表文件缓存空间，返回值为图片存储file_id
    # 3.将得到的file_id作为image的值进行数据新建
    if not ret.get('Status') == 'Upload successed.':
        raise serializers.ValidationError('图片上传失败')
    file_id = ret.get('Remote file_id')
    return file_id