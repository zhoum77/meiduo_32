from fdfs_client.client import Fdfs_client
#1.创建客户端（指定配置文件）
client = Fdfs_client('client.conf')      #选择同一文件夹下的配置文件
#2.上传
# ret = client.upload_appender_by_filename('/home/python/Desktop/01.jpeg')
ret = client.upload_by_filename('/home/python/Desktop/01.jpeg')
print(ret)
#getting connection
# <fdfs_client.connection.Connection object at 0x7f47d10541d0>
# <fdfs_client.fdfs_protol.Tracker_header object at 0x7f47d1054198>
# {'Group name': 'group1', 'Remote file_id': 'group1/M00/00/00/wKgGil3eR36EBjFoAAAAAN0Tziw90.jpeg', 'Status': 'Upload successed.', 'Local file name': '/home/python/Desktop/01.jpeg', 'Uploaded size': '46.00KB', 'Storage IP': '192.168.6.138'}
